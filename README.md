# Informatique de Tronc Commun - 2<sup>ème</sup> année

* [Calendrier des MP](docs/2/calendrier/mp.md)
* [Calendrier des MP*](docs/2/calendrier/mpe.md)
* [Calendrier des PC](docs/2/calendrier/pc.md)

![logo](logo.png)

## Infos générales

* Me contacter : [mon adresse mail](mailto:benouwt.info@gmail.com)
* Le programme : [première & deuxième année](https://prepas.org/index.php?document=72)
* Installer Thonny pour utiliser Python sur son ordinateur : [cliquez ici](https://thonny.org/)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*

