# Chapitre : Principes du hachage

## I. Dictionnaires en Python

### 1. Définition

Ensemble d'associations clé / valeur.

Les dictionnaires sont mutables.

Types autorisés pour les clés.

Syntaxe pour la création, l'ajout, la suppression, la modification, la copie.

### 2. Parcourir un dictionnaire

Exemple de boucle.

Parcours sur les clés, sur les valeurs.

### 3. Test de présence d'une clé

Syntaxe, complexité temporelle.

## II. Principe du hachage

### 1. Fonction de hachage

Exemple introductif : compter les occurrences dans un texte.

Principe de fonctionnement d'une fonction de hachage.

Fonction `hash` en Python.

### 2. Table de hachage

Création de la table de hachage et fonctionnement.

Explications sur les limitations des types possibles pour les clés, et sur la complexité de la recherche d'une clé.

Notion de hash réduit.

### 3. Gestion des collisions

Définition.

Modification de la taille de la table de hachage pour éviter les collisions.

Open addressing en cas de collision.

Conséquence sur la complexité du test de présence d'une clé dans un dictionnaire.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
