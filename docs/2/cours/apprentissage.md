# Chapitre : Algorithmes d'apprentissage

Apprentissage supervisé et apprentissage non supervisé.

## I. Algorithme des `k` plus proches voisins

Il s'agit d'un algorithme d'apprentissage supervisé.

### 1. Principe

Problème : on possède des données d'observations ayant certaines caractéristiques et dont on connaît la classe, on souhaite alors classifier une donnée inconnue.

Exemple visuel :

![](img/algorithmes_apprentissage/knn_ronds_carres.png)

La classe de l'inconnue est déterminée comme étant la classe des `k` données qui lui ressemblent le plus.

Exemple visuel :

![](img/algorithmes_apprentissage/knn_ronds_carres_deroule.png)

### 2. Déroulé de l'algorithme

Différentes étapes de l'algorithme des `k` plus proches voisins (en Python) :

* Normalisation des données.
* Calcul des distances euclidiennes entre l'inconnue et les données.
* Conservation des `k` données les plus proches.
* Détermination de la classe majoritaire parmi les `k` données conservées.

### 3. Phase d'apprentissage

Séparation des données d'observations en 2 groupes : les données d'apprentissage et les données de test.

Application de l'algorithme avec un `k` fixé afin de trouver la classe des données de test (qu'on connaît déjà), stockage des résultats dans une matrice de confusion.

Comparaison des indicateurs de performance obtenus grâce à la matrice de confusion pour plusieurs valeurs de `k`.

## II. Algorithme des `k`-moyennes

Il s'agit d'un algorithme d'apprentissage non supervisé.

### 1. Principe

Problème : on possède des données d'observations ayant certaines caractéristiques mais dont on ne connaît pas la classe, on sait qu'il y a `k` classes différentes possibles pour les données, on souhaite donc regrouper les données semblables en `k` groupes distincts qu'on supposera alors de même classe.

Exemple visuel :

![](img/algorithmes_apprentissage/kmeans_inconnus.png)

Les groupes sont déterminés aléatoirement initialement, puis reformés plusieurs fois en associant les données au centre de la classe la plus proche. On converge vers des minima locaux.

Exemple visuel :

![](img/algorithmes_apprentissage/kmeans_deroule.png)

### 2. Déroulé de l'algorithme.

Différentes étapes de l'algorithme des `k` moyennes (en Python) :

* Normalisation des données.
* Choix aléatoire de `k` données servant de centres provisoires.
* Association de chaque donnée au centre le plus proche en terme de distance euclidienne.
* Calcul des barycentres des classes ainsi formées.
* Recommencer les deux étapes précédentes tant que les centres changent.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*
