# Chapitre : Programmation dynamique

## I. Exemple introductif : Fibonacci

### 1. Récursivité simple et problème rencontré

Définition de la suite de Fibonacci, fonction récursive simple.

Arbres des appels récursifs de la fonction, notion de chevauchement.

Analyse de la complexité temporelle de la fonction récursive simple.

### 2. Une première solution : récursivité avec mémoïsation

Notion de structure de données permettant de stocker les résultats des appels.

Ré-écriture de la fonction récursive de Fibonacci pour utiliser la mémoïsation.

Méthodologie.

### 3. Une seconde solution : version itérative

Calcul des valeurs de la suite « de bas en haut ».

Ré-écriture de la fonction Fibonacci pour obtenir une version itérative.

Méthodologie.

## II. Enjeux de complexité spatiale : Coefficients binomiaux

### 1. Récursivité simple

Formule de Pascal, fonction récursive simple.

Arbre des appels et chevauchements.

### 2. Solution avec un tableau

Tableau bidimensionnel : le résultat de $`\binom i j`$ est stocké ligne `i` et colonne `j`.

Premier code avec mémoïsation et second code de bas en haut.

Complexité spatiale nécessaire.

### 3. Solution avec un dictionnaire

Dictionnaire : le résultat de $`\binom i j`$ est associé à la clé `(i, j)`.

Ré-écriture du code pour utiliser le dictionnaire.

Amélioration de la complexité spatiale.

## III. Formalisation

### 1. Problèmes rencontrés

#### a. Problèmes de combinatoire

Principe, comment reconnaître de tels problèmes ?

Premier exemple : compter le nombre de chemins allant d'une case à une autre dans une grille, formule de récurrence et chevauchement de sous-problèmes.

Deuxième exemple : compter le nombre de manières de paver une zone avec des dominos, formule de récurrence et chevauchement de sous-problèmes.

#### b. Problèmes d'optimisation

Principe, comment reconnaître de tels problèmes ?

Premier exemple : chemin de poids maximal dans une pyramide de nombres, formule de récurrence et chevauchement de sous-problèmes.

Deuxième exemple : plus court chemin dans un graphe pondéré (cf. Floyd-Warshall vu en TP).

### 2. Principe de la programmation dynamique (+ méthodo)

Notion de sous-structure optimale, chevauchements des sous-problèmes.

Méthodes Bottom-up (itérative) et Top-down (récursive avec mémoïsation)

Complexités temporelle et spatiale.

Reconstruction de la solution optimale à partir du résultat calculé.

### 3. Lien avec les algorithmes gloutons

Rappels sur le principe des algorithmes gloutons.

Problème du rendu de monnaie : résolution avec un algorithme glouton, résolution avec la programmation dynamique et comparaison des deux méthodes.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
