# Chapitre : Révisions générales sur les graphes

## I. Représentations

Définition d'un graphe, orienté et non orienté.

Liste d'adjacence d'un graphe : principe, exemple, complexité spatiale.

Matrice d'adjacence d'un graphe : principe, exemple, complexité spatiale.

Codes Python pour convertir une matrice en liste et inversement.

Propriétés sur la somme des degrés dans un graphe.

## II. Caractéristiques des graphes

Vocabulaire des graphes :

* Voisins / prédécesseurs et successeurs
* Degré / degrés entrant et sortant
* Boucle
* Graphe complet
* Chemin
* Cycle
* Connexité
* Distance

Divers codes Python associés à ce vocabulaire.

## III. Parcours de graphes

Algorithme générique pour le parcours :

```
ENTREES :
	- la liste d'adjacence d'un graphe G
	- un sommet de départ d
ALGO :
	a_traiter = pile / file vide
	empiler / enfiler d dans a_traiter
	visités = {}
	TANT QUE a_traiter est non vide :
		s = depiler / defiler un sommet de a_traiter
		SI s n'est pas dans visités :
			ajouter s dans visités
			POUR chaque voisin v de s :
				empiler / enfiler v dans a_traiter
```

On utilise une pile pour le parcours en profondeur, une file pour le parcours en largeur.

Exemples de parcours en profondeur et en largeur.

Complexité du parcours.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
