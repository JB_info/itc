# Chapitre : Bases de données

## I. Organisation d'une base de données

### 1. Conception d'une base

Entités et associations.

Cardinalités des associations : $`1-1, 1-*, *-*`$.

Séparation d'une association de cardinalité $`* - *`$ en deux associations de cardinalités $` 1 - *`$.

### 2. Modèle relationnel

Vocabulaire : relations (tables), attributs (colonnes) et leurs domaines, enregistrements (lignes).

Chaque relation possède une clé primaire : c'est un attribut (ou plusieurs attributs) qui garantit l'unicité de chaque enregistrement.

Les relations peuvent posséder des clés étrangères : c'est un attribut qui fait référence à la clé primaire d'une autre relation.

Schéma relationnel d'une base de données.

![](img/bases_de_donnees/conception.jpg)

## II. Requêtes SQL

Bonnes pratiques sur le format des requêtes SQL.

### 1. Sélection simple

Mots-clefs : `SELECT, FROM, WHERE`.

Opérateurs : `+ - * / % = <> < <= > >= AND OR NOT`.

![](img/bases_de_donnees/select_from.png)

![](img/bases_de_donnees/select_tout.png)

![](img/bases_de_donnees/where.png)

### 2. Formatage

Mots-clefs : `DISTINCT, AS, ORDER BY (ASC / DESC), LIMIT, OFFSET`.

![](img/bases_de_donnees/distinct.png)

![](img/bases_de_donnees/as.png)

![](img/bases_de_donnees/order_by.png)

![](img/bases_de_donnees/limit.png)

![](img/bases_de_donnees/offset.png)

### 3. Opérations ensemblistes

Mots-clefs : `INTERSECT, UNION, EXCEPT`.

Produit cartésien.

![](img/bases_de_donnees/union.png)

![](img/bases_de_donnees/intersect.png)

![](img/bases_de_donnees/except.png)

![](img/bases_de_donnees/produit_cartesien.png)

### 4. Agrégations et groupes

Mots-clefs : `MIN, MAX, SUM, AVG, COUNT, GROUP BY, HAVING`.

![](img/bases_de_donnees/min.png)

![](img/bases_de_donnees/max.png)

![](img/bases_de_donnees/sum.png)

![](img/bases_de_donnees/avg.png)

![](img/bases_de_donnees/count.png)

![](img/bases_de_donnees/group_by.png)

![](img/bases_de_donnees/having.png)

Le `WHERE` filtre les enregistrements (avant que les groupes soient faits), le `HAVING` filtre les groupes.

![](img/bases_de_donnees/where_et_having.png)

### 5. Jointures

Mots-clefs : `JOIN ... ON ...`.

Autojointures.

![](img/bases_de_donnees/join.png)

![](img/bases_de_donnees/autojointure.png)

### 6. Requêtes imbriquées

![](img/bases_de_donnees/imbrique_where.png)

![](img/bases_de_donnees/imbrique_from.png)

### Conclusion

Structure d'une requête SQL :

![](img/bases_de_donnees/memento_sql.png)


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*, Éric Détrez (dernière image)
