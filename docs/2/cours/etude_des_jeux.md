# Chapitre : Étude des jeux

## I. Jeux d'accessibilité

Définition d'un jeu d'accessibilité à deux joueurs : fini, à coups asynchrones, à information complète, sans hasard, sans mémoire, à somme nulle.

Exemples de jeux d'accessibilité.

## II. Algorithme min-max

Le premier joueur cherche à maximiser son gain, et le second joueur cherche à minimiser le gain du premier joueur. Le gain peut être toute valeur de $`\mathbb R\cup\{+\infty,-\infty\}`$.

### 1. Représentation du jeu

Le jeu peut être représenté par un arbre : les nœuds sont les états possibles du jeu ; les fils d'un nœud `n` sont les différents états du jeu qu'il est possible d'obtenir après un coup depuis l'état `n` ; les nœuds à une même profondeur sont contrôlés par le même joueur ; le jeu termine quand on aboutit à une feuille.

Exemple d'arbre pour le jeu du morpion :

![](img/etude_des_jeux/arbre_morpion.png)

### 2. Stratégie optimale

On étiquette chaque nœud `n` de l'arbre par un gain `E(n)` :

* si `n` est une feuille, alors `E(n)` est le gain du premier joueur lorsque la partie termine sur cet état ;
* si `n` est un nœud interne contrôlé par le premier joueur (c'est à son tour de jouer), alors `E(n) = max {E(s), s fils de n}` ;
* si `n` est un nœud interne contrôlé par le second joueur, alors `E(n) = min {E(s), s fils de n}`.

Si le premier (resp. second) joueur joue toujours en essayant de maximiser (resp. minimiser) la quantité `E`, le score final du premier joueur à la fin de la partie est `E(r)`, avec `r` la racine de l'arbre.

Exemple sur un petit arbre.

La profondeur de l'arbre étant généralement trop grande, cette méthode a une complexité trop importante : nous allons réduire la profondeur de l'arbre considéré et utiliser une heuristique pour étiqueter les feuilles.

### 3. Heuristique

Définition d'une heuristique : fonction `h` qui à tout état du jeu `e` associe une valeur dans $`\mathbb R\cup\{+\infty,-\infty\}`$ telle que plus `h(e)` est grande, meilleur est `e` pour le premier joueur, plus `h(e)` est petite, meilleur est `e` pour le second joueur.

Exemples d'heuristiques pour le jeu du Puissance 4.

Mieux on choisit `h`, plus on a de chances de gagner.

### 4. Stratégie avec heuristique

On choisit le nombre de coups qu'on veut considérer = profondeur maximale de l'arbre.

Les étiquettes des feuilles de l'arbre sont maintenant déterminées par l'heuristique.

On détermine les étiquettes des nœuds internes de l'arbre comme précédemment, en remontant profondeur par profondeur des feuilles jusqu'à la racine (en maximisant pour le premier joueur et en minimisant pour le second). On en déduit ensuite la stratégie à suivre.

Exemple de déroulé de l'algorithme à la main sur un petit arbre.

Code correspondant à l'algorithme :

```python
def algo_min_max (etat, profondeur, joueur, h, f):
    """
    :param etat: état actuel du jeu = noeud de l'arbre
    :param profondeur: (int) profondeur de l'arbre à considérer
    :param joueur: (bool) True si c'est au tour du premier joueur et False si c'est au tour du second joueur
    :param h: (fonction) heuristique à utiliser pour étiqueter les feuilles
    :param f: (fonction) donne la liste des fils d'un nœud dans l'arbre = états suivants du jeu
    """
    if profondeur == 0 or f(etat) == []:
        return h(etat)
    elif joueur:
        maxi = -float("inf")
        for fils in f(etat):
            maxi = max(maxi, algo_min_max(fils, profondeur-1, False, h, f))
        return maxi
    else:
        mini = float("inf")
        for fils in f(etat):
            mini = min(mini, algo_min_max(fils, profondeur-1, True, h, f))
        return mini
```

## III. Construction de stratégies gagnantes

On veut savoir s'*il existe* une manière de gagner pour le premier joueur, $`\exists\text{ve}`$, et ce *pour tout* coup du second joueur, $`\forall\text{dam}`$.

### 1. Représentation du jeu

Un graphe G = (S, A) est dit biparti s'il existe une partition de S en deux sous-ensembles $`S_A, S_E`$ telle qu'aucun arc de A ne relie deux sommets de $`S_A`$ ou deux sommets de $`S_E`$.

Le jeu est représenté par un triplet (G, $`S_A`$, $`S_E`$), appelé **arène** où 

* G = (S, A) est un graphe biparti avec $`(S_A, S_E)`$ la partition de S
* chaque sommet représente une **position** = un état du jeu
* $`S_A`$ (resp. $`S_E`$) est l'ensemble des positions pour lesquelles c'est au tour d'Adam (resp. Eve) de jouer
* A représente l'ensemble des coups possibles depuis chaque position du jeu

Une **partie** est définie comme un chemin dans une arène.

Les **états finaux** sont un sous-ensemble de S tels que, si un sommet de ce sous-ensemble est atteint, la partie est terminée (sommets sans successeurs dans l'arène). Il y en a trois types : les états finaux gagnants pour Adam, les états finaux gagnants pour Eve, les états finaux de match nul.

### 2. Stratégie gagnante

Définitions : stratégie, partie jouée suivant une stratégie, stratégie gagnante, position gagnante.

Exemple sur une arène simple.

Pour gagner à un jeu d'accessibilité, il faut : une stratégie gagnante (indique le coup à jouer) + une position gagnante (indique où commencer la partie).

Si la position de départ `d` est fixée :

* le joueur qui commence gagne si `d` est une position gagnante pour ce joueur ;
* soit Eve a une stratégie gagnante, soit c'est Adam.

### 3. Attracteurs

On cherche les positions gagnantes pour Eve :

* Si la partie commence sur un état final de Eve, sa victoire est immédiate : position gagnante en 0 coup.

* Si la partie commence sur un sommet de $`S_E`$ (Eve qui joue) qui possède un arc vers un état final de Eve, sa victoire est assurée : position gagnante en 1 coup.

    Si la partie commence sur un sommet de $`S_A`$ (Adam qui joue) qui possède uniquement des arcs vers des états finaux de Eve, sa victoire est assurée : position gagnante en 1 coup.

* En continuant ainsi, on peut trouver les positions gagnantes pour Eve en 2 coups, puis 3, etc. :

    Si c'est au tour de Eve de jouer, il faut qu'*il existe* un arc vers une position gagnante pour que la position actuelle soit elle aussi gagnante pour Eve.

    Si c'est au tour d'Adam de jouer, il faut que *tous* les arcs mènent à une position gagnante d'Eve pour que la position actuelle soit elle aussi gagnante pour Eve.

On appelle les ensembles ainsi construits des **attracteurs**.

Définition formelle des attracteurs.

Terminaison du calcul des attracteurs.

Exemple de calcul des attracteurs sur une petite arène.

Construction d'une stratégie gagnante à partir du calcul des attracteurs.

### 4. Exemple concret : le jeu de Nim (version Fort Boyard)

Règles du jeu.

Dessin de l'arène avec 9 bâtons de départ.

États finaux pour Eve et pour Adam, pas d'états finaux de match nul.

Calcul à la main des attracteurs.

Stratégie gagnante déterminée à partir des positions gagnantes ainsi trouvées.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *Pyrat* (arbre du morpion)
