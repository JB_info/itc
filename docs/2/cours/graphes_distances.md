# Chapitre : Plus court chemin dans un graphe

## I. Distances dans un graphe non pondéré

Dans un graphe non pondéré, la distance entre un sommet `dep` et un sommet `arr` et le nombre minimal d'arcs (ou arêtes) d'un chemin allant de `dep` à `arr`.

On calcule les distances à l'aide du parcours en largeur.

Algorithme en Python utilisant un parcours en largeur pour calculer des distances.

## II. Distances dans un graphe pondéré

Dans un graphe pondéré, la distance entre un sommet `dep` et un sommet `arr` et la somme minimale des poids des arcs (ou arêtes) d'un chemin allant de `dep` à `arr`.

On calcule les distances à l'aide de l'algorithme de Dijkstra.

Algorithme en Python utilisant Dijkstra pour calculer des distances.

L'algorithme A* utilise une heuristique pour améliorer l'efficacité du calcul des distances, quand le sommet d'arrivée est connu.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
