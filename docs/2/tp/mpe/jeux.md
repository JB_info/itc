# TP : Étude des jeux

L'objectif de ce TP est de travailler les deux algorithmes sur les jeux d'accessibilités vus en cours :

* l'algorithme du min-max sur un arbre
* le calcul des attracteurs sur un graphe biparti

## I. Application des algorithmes

> 1. Rappelez les caractéristiques que doit respecter un jeu pour être qualifié de jeu d'accessibilité.

On considère l'arbre suivant :

![](../img/jeux/min_max.png)

> 2. Appliquez l'algorithme du min-max à cet arbre, en supposant que les sommets carrés appartiennent au joueur cherchant à maximiser son gain (et les sommets ronds à son adversaire donc).
> 3. Appliquez maintenant l'algorithme à cet arbre en supposant que ce sont les sommets ronds qui appartiennent au jour cherchant à maximiser son gain.
> 4. En supposant qu'il s'agit de l'arbre du début d'une partie, est-il plus avantageux pour le joueur qui cherche à maximiser son gain de commencer la partie ou de laisser la main à son adversaire ?

On considère l'arène suivante :

![](../img/jeux/attracteurs.png)

* Les sommets carrés appartiennent au joueur 1 et les sommets ronds au joueur 2.
* Le sommet grisé correspond à l'état initial du jeu.
* Le sommet jaune est un état final gagnant pour le joueur 1, le sommet rose est un état final gagnant pour le joueur 2.

> 5. Calculez les attracteurs pour le joueur 1.
> 6. Calculez les attracteurs pour le joueur 2.
> 7. Quel joueur possède une stratégie gagnante ?

## II. Étude du jeu de Nim

Nous allons dans cette partie chercher à appliquer les deux algorithmes étudiés sur le *jeu de Nim*.

On considère la version simple du jeu de Nim : au début de la partie, les deux joueurs ont face à eux un tas de $`n\in \mathbb N^*`$ bâtons. Tour à tour, les deux joueurs retirent soit 1, soit 2, soit 3 bâtons du tas. Le joueur qui retire le dernier bâton perd la partie.

Voici un exemple de partie possible avec 7 bâtons et le joueur 1 qui commence :

* Le joueur 1 retire 2 bâtons, il en reste 5.
* Le joueur 2 retire 3 bâtons, il en reste 2.
* Le joueur 1 retire 1 bâton, il en reste 1.
* Le joueur 2 retire le dernier bâton, il a perdu.

> 1. Vérifiez qu'il s'agit bien d'un jeu d'accessibilité.

Nous allons représenter les états de la partie par des couples `(n, j)` avec `n` le nombre de bâtons restants et `j` qui vaut 1 si c'est au tour du premier joueur ou 2 si c'est au tour du second joueur.

### Min-max

Commençons par utiliser l'algorithme du min-max (sans heuristique) afin de déterminer si le joueur 1 qui commence peut gagner.

On décide d'étiqueter une feuille de l'arbre avec la valeur $`+\infty`$ si le joueur 1 a gagné lorsque la partie se termine sur cette feuille, et $`-\infty`$ sinon.

*Remarque : ce jeu ne peut pas se terminer sur une égalité.*

> 2. * Dessinez l'arbre obtenu pour une partie avec 5 bâtons. Le nœud à la racine sera donc `(5, 1)` car il reste 5 bâtons et c'est au tour du joueur 1.
>     * Étiquetez les feuilles de l'arbre avec la bonne valeur.
>     * Appliquez à la main l'algorithme du min-max afin d'étiqueter les nœuds internes.
>     * Déterminez si le joueur 1 peut gagner.
>
> 3. Écrivez une fonction `successeurs(etat_courant)` qui renvoie la liste des états possibles de la partie après avoir joué 1 coup à partir de l'état courant. Par exemple :
>
>     ```python
>     >>> successeurs((5, 1))
>     [(4,2), (3,2), (2,2)]
>     >>> successeurs((2, 2))
>     [(1,1), (0,1)]
>     ```
>
> 4. Écrivez une fonction `gain(etat_final)` qui renvoie le gain du joueur 1 pour un état final donné. Par exemple :
>
>     ```python
>     >>> gain((0, 1))
>     inf
>     ```
>
>     On lèvera une exception si l'état donné n'est pas un état final (c'est-à-dire s'il reste plus de 0 bâton).
>
> 5. Écrivez une fonction `min_max_nim(etat)` qui prend en paramètre l'état courant de la partie et renvoie son étiquette, déterminée avec l'algorithme du min-max sans heuristique.
>
>     Par exemple :
>
>     ```python
>     >>> min_max_nim((5, 1))
>     -inf
>     ```
>
> 6. Déduisez-en une fonction `possede_strategie_gagnante(n)` qui prend en paramètre le nombre initial de bâtons de la partie et renvoie un booléen indiquant si le joueur 1 possède une stratégie gagnante.
>
>     Vous pouvez placer les lignes suivantes dans votre fichier pour vérifier que tout est correct :
>
>     ```python
>     assert possede_strategie_gagnante(2)
>     assert possede_strategie_gagnante(3)
>     assert possede_strategie_gagnante(4)
>     assert not possede_strategie_gagnante(5)
>     assert possede_strategie_gagnante(6)
>     assert possede_strategie_gagnante(7)
>     assert possede_strategie_gagnante(8)
>     assert not possede_strategie_gagnante(9)
>     ```
>
> 7. Ces résultats vous semblent-ils cohérents ?
>
> 8. Essayez de lancer la fonction avec un nombre de bâtons plus grand (30 par exemple). Que se passe-t-il ? Pourquoi ?
>
> 9. Quelle est la profondeur maximale de l'arbre avec `n` bâtons au départ ?

### Calcul des attracteurs

On souhaite maintenant utiliser l'algorithme de calcul des attracteurs afin de vérifier qu'on trouve bien le même résultat.

> 10. * Dessinez le graphe biparti correspondant à une partie avec 8 bâtons.
>     * Quels sont les états finaux ?
>     * Calculez à la main les attracteurs du joueur 1.
>     * Le joueur 1 possède-t-il une stratégie gagnante ?
>
> 11. Écrivez une fonction `tous_sommets(nb_batons)` qui prend en paramètre le nombre de bâtons initial du jeu et renvoie la liste de tous les sommets de l'arène.
>
> 12. Écrivez une fonction `existe_appartenance(l1, l2)` qui renvoie `True` s'il existe un élément de la liste `l1` appartenant à la liste `l2`, et `False` sinon.
>
> 13. Écrivez une fonction `tous_appartenance(l1, l2)` qui renvoie `True` si tous les éléments de la liste `l1` appartiennent à la liste `l2`, et `False` sinon. Si la liste `l1` est vide, on renverra `False`.
>
> 14. Écrivez une fonction `attracteurs_suivants(sommets, A)` qui prend en paramètres la liste des sommets du graphe et la liste actuelle des attracteurs du joueur 1 et calcule les nouveaux attracteurs du joueur 1. Il faut donc renvoyer une liste contenant :
>
>     * les sommets du graphe appartenant au joueur 1 pour lesquels il existe un de leurs successeurs dans A ;
>     * les sommets du graphe appartenant au joueur 2 pour lesquels tous leurs successeurs sont dans A.
>
>     Par exemple :
>
>     ```python
>     >>> attracteurs_suivants(tous_sommets(4), [(0, 1)])
>     [(1, 2)]
>     >>> attracteurs_suivants(tous_sommets(4), [(0, 1), (1, 2)])
>     [(2, 1), (3, 1), (4, 1)]
>     ```
>
> 15. Déduisez-en une fonction `calcul_attracteurs(n)` qui calcule tous les attracteurs du joueur 1 pour une partie avec `n` bâtons. Au départ, seuls les états finaux gagnants pour le joueur 1 sont dans les attracteurs. On calcule ensuite les attracteurs successifs jusqu'à ce que l'ensemble se stabilise (c'est-à-dire jusqu'à ce que le nombre d'attracteurs cesse de changer).
>
> 16. À l'aide de cette fonction, déterminez à la main, si elle existe, quelle est la stratégie gagnante du joueur 1.
>
> 17. Déduisez-en une fonction `possede_strategie_gagnante_v2(n)` qui prend en paramètre le nombre initial de bâtons de la partie et renvoie un booléen indiquant si le joueur 1 possède une stratégie gagnante.
>
> 18. Vérifiez qu'on retrouve bien les mêmes résultats qu'avec le min-max :
>
>     ```python
>     assert possede_strategie_gagnante_v2(2)
>     assert possede_strategie_gagnante_v2(3)
>     assert possede_strategie_gagnante_v2(4)
>     assert not possede_strategie_gagnante_v2(5)
>     assert possede_strategie_gagnante_v2(6)
>     assert possede_strategie_gagnante_v2(7)
>     assert possede_strategie_gagnante_v2(8)
>     assert not possede_strategie_gagnante_v2(9)
>     ```

## III. Un jeu nécessitant une heuristique : le puissance 4

On s'intéresse dans cette partie au jeu du *Puissance 4*.

L'objectif des joueurs est d'aligner 4 de leurs pions, dans une grille de taille $`n \times m`$. Les alignements peuvent se faire en diagonale, à l'horizontale ou à la verticale. Chacun leur tour, les joueurs déposent un unique pion dans la colonne de leur choix, et automatiquement ce pion descend jusqu'à ligne vide la plus basse. Si vous le souhaitez, cliquez [ici](https://lululataupe.com/tout-age/686-puissance-4) pour faire une partie en ligne du puissance 4.

Le jeu de Puissance 4, même dans sa version « réelle » avec 7 colonnes et 6 lignes, comporte beaucoup trop de configurations différentes pour l’approche par le calcul des attracteurs. On va donc traiter la recherche de stratégie avec une heuristique, et avec l’algorithme min-max.

Nous représenterons une grille de puissance 4 ainsi :

* la grille sera une liste de listes ;
* les cases vides seront symbolisées par un 0 ;
* les cases contenant un pion du joueur 1 seront symbolisées par un 1 ;
* les cases contenant un pion du joueur 2 seront symbolisées par un 2.

Voici deux exemples de grille :

```python
grille_exemple = [[0, 0, 0, 2, 0, 0, 0],
                  [0, 0, 0, 2, 1, 0, 0],
                  [0, 0, 1, 1, 2, 0, 0],
                  [0, 0, 1, 2, 1, 0, 0],
                  [0, 1, 1, 2, 1, 2, 0],
                  [0, 1, 2, 1, 2, 2, 0]]

grille_exemple2 = [[0, 2, 1, 2, 1, 0, 2],
                   [0, 1, 2, 2, 1, 0, 1],
                   [0, 2, 1, 1, 2, 0, 2],
                   [0, 1, 1, 2, 1, 0, 1],
                   [0, 2, 1, 2, 1, 2, 2],
                   [0, 1, 2, 1, 2, 2, 1]]
```

On fournit la fonction suivante, qui renvoie 1 si la grille est gagnante pour le joueur 1 (4 pions du joueur 1 alignés), 2 si la grille est gagnante pour le joueur 2, ou 0 si la grille n'est gagnante pour personne (aucun des deux joueurs a 4 pions alignés) :

```python
def grille_gagnante(grille):
    for ligne in range(6): # lignes
        for colonne in range(4):
            if grille[ligne][colonne] == grille[ligne][colonne + 1] == grille[ligne][colonne + 2] == grille[ligne][colonne + 3] and grille[ligne][colonne] != 0:
                return grille[ligne][colonne]
    for colonne in range(7): # colonnes
        for ligne in range(3):
            if grille[ligne][colonne] == grille[ligne + 1][colonne] == grille[ligne + 2][colonne] == grille[ligne + 3][colonne] and grille[ligne][colonne] != 0:
                return grille[ligne][colonne]
    for ligne in range(3): # diagonales \
        for colonne in range(4):
            if grille[ligne][colonne] == grille[ligne + 1][colonne + 1] == grille[ligne + 2][colonne + 2] == grille[ligne + 3][colonne + 3] and grille[ligne][colonne] != 0:
                return grille[ligne][colonne]
    for ligne in range(3): # diagonales /
        for colonne in range(3, 7):
            if grille[ligne][colonne] == grille[ligne + 1][colonne - 1] == grille[ligne + 2][colonne - 2] == grille[ligne + 3][colonne - 3] and grille[ligne][colonne] != 0:
                return grille[ligne][colonne]
    return 0 # aucun alignement trouvé
```

> 1. Testez :
>
>     ```python
>     >>> grille_gagnante(grille_exemple)
>     ?
>     >>> grille_gagnante(grille_exemple2)
>     ?
>     ```

Nous allons utiliser une heuristique pour étiqueter les feuilles de l'arbre lors de l'algorithme du min-max.

Pour cela, nous allons considérer le nombre d'alignements potentiels de chaque case de la grille. Par exemple, pour la case tout en bas à gauche, il y a 3 alignements potentiels impliquant cette case (1 colonne, 1 ligne, 1 diagonale). Pour la case tout en bas, colonne du milieu, il y a 7 alignements potentiels (1 colonne, 4 lignes, 2 diagonales).

> 2. Complétez la matrice suivante, qui donne le nombre d'alignements potentiels de chaque case d'une grille de Puissance 4 :
>
>     ```python
>     nb_alignements = [[3, 4, 5, 7, 5, 4, 3],
>                       [?, ?, ?, ?, ?, ?, ?],
>                       [5, 8, 11, 13, 11, 8, 5],
>                       [5, 8, 11, 13, 11, 8, 5],
>                       [?, ?, ?, ?, ?, ?, ?],
>                       [3, 4, 5, 7, 5, 4, 3]]
>     ```

L'heuristique pour une grille `g` vaut :

* $`+\infty`$ s'il y a déjà 4 pions du joueur 1 aligné dans `g` ;
* $`-\infty`$ s'il y a déjà 4 pions du joueur 2 aligné dans `g` ;
* la somme des alignements potentiels des cases où il y a un pion du joueur 1, moins la somme des alignements potentiels des cases où il y a un pion du joueur 2 sinon.

L'heuristique donne donc des valeurs élevées pour le joueur 1 qui cherche à maximiser son score si l'état de la partie semble lui être favorable, et des valeurs basses si l'état de la partie semble favorable à son adversaire.

> 3. Écrivez une fonction `heuristique(grille)` qui prend en paramètre une grille et renvoie la valeur de l'heuristique associée à cette grille, calculée comme décrit ci-dessus. Par exemple :
>
>     ```python
>     >>> heuristique(grille_exemple)
>     inf
>     >>> heuristique(grille_exemple2)
>     8
>     ```

Nous allons maintenant implémenter les fonctions nécessaires aux manipulations de la grille du jeu.

Pour afficher proprement une grille, on pourra utiliser la fonction suivante :

```python
def afficher_grille(grille):
    for i in range(6):
        print("|", end="")
        for j in range(7):
            if grille[i][j] == 0:
                print(" |", end="")
            else:
                print(str(grille[i][j])+"|", end="")
        print()
    print("-"*15, end='\n\n')
```

> 4. Écrivez une fonction `ajoute_pion(grille, joueur, colonne)` qui renvoie la grille dans laquelle un pion du joueur donné a été ajouté dans la colonne choisie. La grille renvoyée doit être indépendante de celle donnée en paramètre (pas d'effet de bord, il faut copier la grille d'origine). Par exemple :
>
>     ```python
>    >>> afficher_grille(ajoute_pion(grille_exemple, 2, 0))
>     | | | |2| | | |
>     | | | |2|1| | |
>     | | |1|1|2| | |
>     | | |1|2|1| | |
>     | |1|1|2|1|2| |
>     |2|1|2|1|2|2| |
>     ---------------
>     >>> afficher_grille(ajoute_pion(grille_exemple, 2, 1))
>     | | | |2| | | |
>     | | | |2|1| | |
>     | | |1|1|2| | |
>     | |2|1|2|1| | |
>     | |1|1|2|1|2| |
>     | |1|2|1|2|2| |
>     ---------------
>    ```
> 
> 5. Écrivez une fonction `colonnes_possibles(grille)` qui renvoie la liste des numéros de colonnes dans lesquelles on peut encore jouer. Si la grille donnée était déjà gagnante pour un des deux joueurs, il n'y a aucun coup possible donc on renvoie une liste vide.
>
>     ```python
>    >>> colonnes_possibles(grille_exemple)
>     []
>     >>> colonnes_possibles(grille_exemple2)
>     [0, 5]
>    ```

On a maintenant tout ce qu'il nous faut pour implémenter l'algorithme du min-max.

> 6. Complétez la fonction suivante, qui renvoie l'étiquette associée à la grille par l'algorithme du min-max, ainsi que la colonne dans laquelle l'algorithme conseille de jouer :
>
>     ```python
>     def min_max_puissance4(grille, joueur, profondeur):
>         # feuilles (profondeur 0 ou plus de coups possibles)
>         if ?:
>             return heuristique(grille), None
>         # noeuds internes, tour du joueur 1 qui maximise
>         elif joueur == 1:
>             etiquette_maxi = None
>             coup_a_jouer = None
>             for col in colonnes_possibles(grille):
>                 etiq, _ = min_max_puissance4(?, ?, ?)
>                 if etiquette_maxi == None or etiquette_maxi < etiq:
>                     etiquette_maxi = etiq
>                     coup_a_jouer = col
>             return etiquette_maxi, coup_a_jouer
>         # noeuds internes, tour du joueur 2 qui minimise
>         else:
>             ?
>     ```
>
>     Pour tester votre fonction :
>
>     ```python
>     >>> min_max_puissance4(grille_exemple2, 1, 3)
>     (14, 5)
>     >>> min_max_puissance4(grille_exemple2, 1, 5)
>     (-inf, 0)
>     ```
>
> 7. Vérifiez qu'avec un arbre de profondeur maximale 3, le coup conseillé pour une grille vide (début de jeu) est la colonne du milieu.
>
> 8. Écrivez une fonction `partie(profondeur)` qui effectue une partie de Puissance 4 où les deux adversaires choisissent leurs coups d'après l'algorithme du min-max avec la profondeur donnée. Le joueur 1 commence. On affichera la grille à chaque tour.
>
> 9. Testez avec plusieurs profondeurs : 3, 4, 5, 6.
>
>     Avec `p = 6`, la partie se solde sur une égalité... Mais c'est un peu long.
>

## Pour aller plus loin

> 1. Comparez les temps d'exécution des deux fonctions qui déterminent si le joueur 1 possède une stratégie gagnante au jeu de Nim (avec le min-max / avec le calcul des attracteurs).
> 1. Modifiez la fonction `partie` du puissance 4 pour lancer des parties humain VS intelligence artificielle. Êtes-vous plus fort que la machine au puissance 4 ? ... jusqu'à quelle profondeur de l'arbre ?
> 1. Pour éviter de calculer entièrement l'heuristique à chaque coup au puissance 4, on remarque que si on connaît la valeur `h` de l'heuristique pour la grille actuelle, alors il n'y a qu'une addition (resp. soustraction) à faire si c'est au tour du joueur 1 (resp. du joueur 2) pour connaître la nouvelle valeur de l'heuristique. Modifiez votre code en conséquence.
> 1. Réfléchissez à d'autres heuristiques pour le min-max.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
