# TP : Bases de données (SQL)

À l'issue de ce TP, vous devez être en mesure d'écrire des requêtes SQL utilisant l'ensemble des mots-clefs. Dans quasiment tous les sujets de concours, on vous demande d'écrire 4 / 5 requêtes SQL : c'est un incontournable et ce sont des points facilement gagnés.

## I. Premières requêtes

### 1. Présentation de la base

> 1. Téléchargez le fichier contenant la base de données : [peintures.db](../code/peinture.db).
> 2. Ouvrez le logiciel «DB Browser for SQLite », puis ouvrez la base `peintures` (onglet « Ouvrir une base de données » en haut à gauche).

La base de données avec laquelle nous travaillerons pour ce TP est constituée de 5 tables : `Oeuvres, Peintres, Musees, Villes, Pays`.

> 3. Exécutez la requête SQL suivante, que fait-elle ?
>
>     ```sql
>     SELECT *
>     FROM Pays
>     ```
>     
> 4. Avec des requêtes similaires, visualisez les données des 4 autres tables de la base.

Vous devez avoir remarqué une valeur particulière, pour les noms communs des peintres par exemple : `NULL`. Cette valeur représente l'absence de donnée.

> 5. Exécutez la requête suivante, et regardez le résultat : y a-t-il des peintres qui n'ont pas d'année de mort ?
>
>     ```sql
>     SELECT id, annee_mort
>     FROM Peintres
>     ```
>

Remarque pratique : on peut écrire un commentaire en SQL en plaçant deux tirets au début de la ligne. Par exemple :

```sql
-- Récupère sans doublon les identifiants des pays d'origine des peintres :
SELECT DISTINCT id_pays
FROM Peintres
```

### 2. Formatage et opérations ensemblistes

On rappelle que le mot-clef permettant de filtrer les résultats selon une certaine condition est `WHERE`. On dispose des opérateurs suivants :

* sur les nombres : `+ - * / %` (comme en Python)
* sur le texte : `||` (concaténation)
* pour comparer : `=` (égal) `<>` (différent) `< <= > >=` (comme en Python)
* pour lier plusieurs conditions : `AND OR NOT`
* tester si une donnée est vide ou non : `IS NULL`, `IS NOT NULL` (attention `=` ne fonctionne pas)

> 6. Exécutez la requête SQL suivante, que fait-elle ?
>
>     ```sql
>     SELECT prenom || ' ' || nom, annee_mort - annee_naissance, id_pays
>     FROM Peintres
>     WHERE (nom_commun IS NULL OR id_pays <> 3) AND annee_mort IS NOT NULL
>     ```

Il est en plus possible de comparer deux textes à l'aide du mot-clef `LIKE` et de deux symboles :

* `_` remplace n'importe quel caractère ;
* `%` remplace un nombre quelconque de caractères.

> 7. Exécutez la requête SQL suivante, que fait-elle ?
>
>     ```sql
>     SELECT *
>     FROM Oeuvres
>     WHERE nom LIKE 'Le%'
>     ```


On peut formater un résultat avec `AS` (renommage d'une colonne), `ORDER BY` (tri du résultat), `LIMIT n` (ne prendre que les `n` premiers résultats), `OFFSET m` (ignorer les `m` premiers résultats).

> 8. Exécutez la requête SQL suivante, que fait-elle ?
>
>     ```sql
>     SELECT id AS 'troisieme peintre mort le plus vieux', annee_mort - annee_naissance AS 'duree_vie'
>     FROM Peintres
>     WHERE annee_mort IS NOT NULL
>     ORDER BY duree_vie DESC
>     LIMIT 1
>     OFFSET 2
>     ```

Il existe 3 opérateurs ensemblistes permettant de relier des requêtes : `UNION, INTERSECT, EXCEPT`.

> 9. Exécutez la requête SQL suivante, que fait-elle ?
>
>     ```sql
>     SELECT prenom
>     FROM Peintres
>         UNION
>     SELECT nom
>     FROM Peintres
>     ```
>
> 10. Écrivez une requête qui récupère les noms des villes qui sont aussi des noms de pays.

### 3. Fonctions d'agrégation et groupes

Les fonctions d'agrégation existantes en SQL sont `MIN, MAX, SUM, AVG, COUNT`.

> 11. Exécutez la requête SQL suivante, que fait-elle ?
>
>     ```sql
>     SELECT AVG(annee_mort - annee_naissance)
>     FROM Peintres
>     ```
>
> 12. Laquelle des deux requêtes suivantes permet de déterminer le nombre de musées qui exposent des œuvres ?
>
>     ```sql
>     SELECT COUNT(id_musee)
>     FROM Oeuvres
>     ```
>
>     ```sql
>     SELECT COUNT(DISTINCT id_musee)
>     FROM Oeuvres
>     ```
>

On peut regrouper les lignes du résultat qui ont la même valeur pour une colonne avec `GROUP BY`.  
Les fonctions d'agrégation s'appliquent alors à chaque groupe et non plus à tout le résultat.  
Il est possible ensuite de ne sélectionner que les groupes respectant certains critères avec `HAVING`.

> 13. On peut compter le nombre de musée dans chaque ville ainsi, testez :
>
>     ```sql
>     SELECT id_ville, COUNT(*) AS 'nombre de musées dans cette ville'
>     FROM Musees
>     GROUP BY id_ville
>     ```
>     
>    Plutôt que d'être effectuée sur toute la table, la fonction d'agrégation `COUNT` est appelée sur chaque groupe. Ainsi on ne compte pas tous les musées existants, on compte en fait pour chaque groupe de musée (ceux ayant la même ville) le nombre de musées du groupe.
>     
>14. Exécutez la requête SQL suivante, que fait-elle ?
> 
>    ```sql
>     SELECT id_pays, MIN(annee_naissance)
>     FROM Peintres
>     GROUP BY id_pays
>     HAVING MIN(annee_naissance) >= 1600
>    ```

On peut bien évidemment écrire des requêtes ayant à la fois un `WHERE` et un `HAVING` :

* le `WHERE` filtre les *lignes* selon certains critères *AVANT* que les groupes soient faits ;
* le `HAVING` filtre les *groupes* selon certains critères *APRÈS* que les groupes soient faits. 

> 15. Exécutez la requête SQL suivante, que fait-elle ?
>
>     ```sql
>     SELECT id_ville
>     FROM Musees
>     WHERE nom <> 'Musée du Louvre'
>     GROUP BY id_ville
>     HAVING COUNT(*) >= 2
>     ```
>
> 16. * Écrivez une requête qui affiche, pour chaque année de mort, le nombre de peintres morts cette année là.
>     * Modifiez la requête pour n'afficher que les années de mort pour lesquelles la moyenne des années de naissance du groupe se situe entre 1400 et 1600.
>     * Modifiez la requête pour ne prendre en compte que les peintres possédant un nom commun.

Il est possible de faire des groupes sur la base de plusieurs données en commun également.

> 17. Testez :
>
>     ```sql
>     SELECT id_peintre, id_musee, COUNT(id) AS 'nombre d''oeuvres du même peintre exposées dans le même musée'
>     FROM Oeuvres
>     WHERE id_musee IS NOT NULL
>     GROUP BY id_peintre, id_musee
>     ```
> 

### 4. Jointures

Si on a besoin de données réparties sur plusieurs tables, il faut réaliser une jointure avec `JOIN`. La jointure se fait sur la colonne que les tables ont en commun.

> 18. Exécutez la requête SQL suivante, que fait-elle ?
>
>     ```sql
>     SELECT *
>     FROM Musees
>     JOIN Villes ON Musees.id_ville = Villes.id
>     ```
>

Comme on a 2 tables différentes dans la requête, on doit préciser d'où vient la donnée utilisée : on est obligés par exemple d'écrire *Villes.id* et non juste *id* car les deux tables *Musees* et *Villes* possèdent une colonne de ce nom et il y aurait ambiguïté. C'est là que va nous être utile le renommage !

> 19. Testez :
>
>     ```sql
>     SELECT *
>     FROM Musees AS M
>     JOIN Villes AS V ON M.id_ville = V.id
>     ```

Si on souhaite relier deux tables qui n'ont pas de lien direct, on peut passer par d'autres tables intermédiaires. Par exemple si on souhaite relier *Musees* à *Pays* qui n'ont pas de lien direct, il suffit de joindre *Musees* à *Villes* puis de joindre *Villes* à *Pays*.

> 20. Testez :
>
>     ```sql
>     SELECT M.nom, V.nom, P.nom
>     FROM Musees AS M
>     JOIN Villes AS V ON M.id_ville = V.id
>     JOIN Pays AS P ON V.id_pays = P.id
>     ```
>
> 21. Exécutez la requête SQL suivante, que fait-elle ?
>
>     ```sql
>     SELECT O.nom, Pays1.nom AS 'pays d''origine du peintre', Pays2.nom AS 'pays d''exposition de l''oeuvre'
>     FROM Oeuvres AS O
>     JOIN Peintres AS P ON O.id_peintre = P.id
>     JOIN Pays AS Pays1 ON P.id_pays = Pays1.id
>     JOIN Musees AS M ON O.id_musee = M.id
>     JOIN Villes AS V ON M.id_ville = V.id
>     JOIN Pays AS Pays2 ON V.id_pays = Pays2.id
>     ```

Lorsqu'on relie une table à elle-même (comme ci-dessus avec *Pays*), on parle d'**autojointure**.

Outre les jointures, un autre moyen de relier deux tables est de faire un produit cartésien : chaque ligne de la première table est juxtaposée à chaque ligne de la seconde.

> 22. Testez :
>
>     ```sql
>     SELECT *
>     FROM Musees, Villes
>     ```

Les produits cartésiens ne sont pas très utiles en pratique car ils ne permettent pas de relier deux tables de manière cohérente avec les données.

### 5. Requêtes imbriquées

Pour finir, il est possible de créer des requêtes plus complexes en imbriquant une « sous-requête » dans la requête principale. Pour cela, on place la sous-requête entre parenthèses.

Par exemple, si on veut le nom et prénom du peintre naît en premier, il faut :

* d'abord trouver la plus petite année de naissance (ce sera la sous-requête) :

    ```sql
    SELECT MIN(annee_naissance)
    FROM Peintres
    ```
    
* puis identifier le peintre correspondant :

     ```sql
     SELECT nom, prenom
     FROM Peintres
     WHERE annee_naissance = (	SELECT MIN(annee_naissance)
     							FROM Peintres  )
     ```

> 23. Exécutez la requête SQL suivante, que fait-elle ?
>
>     ```sql
>     SELECT *
>     FROM Peintres
>     WHERE annee_mort - annee_naissance >= (SELECT AVG(annee_mort - annee_naissance)
>                                            FROM Peintres)
>     ```

**On ne peut pas enchaîner des fonctions d'agrégation** (par exemple *on ne peut pas* écrire `SELECT MAX(COUNT(id))`), on est alors obligés d'imbriquer des requêtes.

> 24. Exécutez la requête SQL suivante, que fait-elle ?
>
>     ```sql
>     SELECT MAX(nb_oeuvres)
>     FROM (	SELECT id_peintre, COUNT(id) AS 'nb_oeuvres'
>     		FROM Oeuvres
>             GROUP BY id_peintre    )
>     ```
>
> 25. Exécutez alors la requête SQL suivante, que fait-elle ?
>
>     ```sql
>     SELECT P.nom, P.prenom
>     FROM Peintres AS P
>     JOIN Oeuvres AS O ON P.id = O.id_peintre
>     GROUP BY O.id_peintre
>     HAVING COUNT(O.id) = (	SELECT MAX(nb_oeuvres)
>     						FROM (	SELECT id_peintre, COUNT(id) AS nb_oeuvres
>     								FROM Oeuvres
>     								GROUP BY id_peintre    )      )
>     ```



## II. Requêtes quelconques

Vous connaissez maintenant tous les mots-clefs SQL permettant d'écrire des requêtes : `SELECT, DISTINCT, FROM, WHERE, AND, OR, NOT, IS (NOT) NULL, AS, ORDER BY, LIMIT, OFFSET, UNION, INTERSECT, EXCEPT, MIN, MAX, SUM, AVG, COUNT, GROUP BY, HAVING, JOIN`.

Une requête s'organise alors ainsi :

![](../img/bdd/memento_sql.png)

Attention, l'ordre des mots-clefs est important.

À vous maintenant de trouver les bonnes requêtes permettant de répondre aux questions suivantes ! **C'est ce type de questions que vous aurez aux concours.**

Même si ce n'est pas précisé, on attend de vous **un renommage systématique des colonnes du résultat** si c'est nécessaire. C'est un peu comme donner des bons noms à vos variables en Python : ça facilite la lecture de votre requête.

Sans plus de précision, vous devez sélectionner toutes les colonnes (avec `SELECT *`).

> **Difficulté** ★ (CCINP)
>
> 1. Quels sont, sans doublon, les noms des musées ?
> 2. Quelles sont les œuvres qui ne sont pas exposées ?
> 3. Quels sont, sans doublon, les deux derniers chiffres des années de naissance des peintres ?
> 4. Quel est le nom complet (nom et prénom concaténés) et nom commun des peintres qui en ont un ?
> 5. Quels sont les cinq derniers peintres morts ?
> 6. Quels sont, suivant l’ordre lexicographique croissant de leur nom, les troisième, quatrième et cinquième pays ?
> 7. Quels sont les prénoms, noms, et durées de vie des peintres qui sont morts avant leur quarantième anniversaire ?
> 8. Quels sont les prénoms et noms des peintres qui n’ont vécu qu’au XVIIIe siècle ou dont l’identifiant du pays est 1 (la France) en ayant vécu au moins 70 ans ?
> 9. Quelles sont les années qui ont vu à la fois la naissance et la mort d’un peintre ?
> 10. Combien de villes possèdent un musée ?
> 11. Combien de villes y a-t-il dans chaque pays ?
> 12. Quels sont les identifiants des pays ayant au moins 2 villes ?
> 13. Quels sont les noms des peintres avec le nom de leurs œuvres ?
> 14. Quels sont les noms des œuvres exposées au Musée du Louvre ?
> 15. Quels sont les nom et prénom du peintre naît en dernier ? On donnera deux requêtes différentes pour cette question.

> **Difficulté** ★★ (Mines-Ponts)
>
> 1. Quel est l’ensemble des noms qui sont des noms de famille ou des noms communs attribués à au moins un peintre ?
> 2. Quelles sont les années de morts de peintres qui ne sont pas aussi des années de naissance d'autres peintres ?
> 3. Quels sont les œuvres actuellement exposées dont le nom comporte deux *e* encadrant exactement 2 caractères ?
> 4. Quel est le nom complet (prénom et nom concaténés) des peintres dont le prénom contient un 'a', le nom ne commence pas par 'Ve' et dont l’avant dernière lettre est un 'i' ?
> 5. Combien de villes ont un nom qui finit par un *e* ou par un *s* ?
> 6. Combien y a-t-il de peintres ayant vécu à partir du XVIème siècle dont le nom contient la particule *di* ou *van* ?
> 7. Quels sont les noms des œuvres et de leurs peintres ? On triera le résultat par date de naissance des peintres dans l’ordre décroissant puis, pour une même date de naissance du peintre, par ordre lexicographique croissant du nom de l’œuvre.
> 8. Combien d’œuvres sont exposées au musée *Unterlinden* ?
> 9. Pour chaque œuvre, quel est son nom, et le nom du pays qui l'expose ?
> 10. Quels sont, sans doublon, les identifiants et noms des musées qui exposent au moins une œuvre du peintre de nom commun Le Titien ?
> 11. Pour chaque peintre, quel est son prénom, son nom et son nombre d’œuvres ?
> 12. Pour chaque pays, quel est son nom, le nombre de peintres qui en sont originaires et la durée moyenne de vie d’un peintre originaire de ce pays ?
> 13. Quels sont les noms des musées exposant au moins 3 œuvres ?
> 14. Y a-t-il des peintres ayant le même prénom et étant originaire du même pays ?
> 15. Quels sont les noms et prénoms des peintres nés le plus récemment ?
> 16. Combien de peintres sont nés après l’année moyenne de naissance d’un peintre ?

> **Difficulté** ★★★ (X/ENS)
>
> 1. Pour chaque peintre, quel est son identifiant, et son nom d'usage s'il en a un / nom de famille sinon ? Le résultat doit avoir deux colonnes (une pour l'identifiant, une pour le nom).
> 2. Quels sont les noms des pays qui contiennent un musée mais dont aucun peintre n’est originaire ?
> 3. Quels sont les prénoms et noms de peintres, ainsi que les noms des pays dans lesquels un peintre a pu voyager, sachant que l’on ne prend pas en compte un pays dont le peintre est originaire ?
> 4. Quels sont les identifiants des pays pour lesquels aucun peintre originaire de ce pays n’est né après 1800 ?
> 5. Quels sont (sans doublon) les noms des pays dont au moins un peintre est originaire et qui contiennent au moins un musée ?
> 6. Quels sont les noms des musées qui exposent au moins deux tableaux dont le titre commence par un 'L' ?
> 7. Pour chaque musée dont le nom commence par *G* et qui expose au moins 2 œuvres de peintres nés après 1480, quel est son nom et le nombre de peintres nés après 1480 qui y sont exposés ?
> 8. Quel est le nom d’un musée qui expose le moins d’œuvres ?
> 9. Quels sont (sans doublon) les noms de tableaux qui sont ceux d’au moins deux œuvres différentes ?
> 10. Quels sont les identifiants, prénoms et noms des peintres originaires du même pays que le peintre connu sous le nom commun *Sandro Botticelli* ?
> 11. Quel est le nombre maximal d’œuvres peintes par un même artiste ?
> 12. Quel est le nombre moyen d’œuvres dans les musées qui comportent au moins deux œuvres ?
> 13. Quels sont (sans doublon) les noms des peintres français et les noms des musées français qui exposent au moins une de leurs œuvres ?
> 14. Quels sont (sans doublon) les noms des peintres qui sont exposés dans un musée dans lequel il y a au moins un autre peintre de même nationalité que ce peintre qui est exposé ?
> 15. Quel est le nom des peintres qui ont peint au moins une œuvre exposée dans un pays dans lequel est exposé dans une autre ville de ce pays au moins une œuvre d’un peintre qui est né ou mort strictement plus tôt ?
> 16. Quels sont les noms et prénoms des peintres dont aucune œuvre n’est exposée ?
> 17. Quels sont les noms des peintres exposés dans tous les musées parisiens ?



## Pour aller plus loin

> **Vos propres requêtes !**
>
> 1. Inventez vos propres questions concernant la base des peintures, et demandez à votre voisin d'y répondre avec une requête SQL !
>
> **Un RPG en SQL !**
>
> Vous avez survécu à un crash d’avion, et vous vous retrouvez coincé sur une île : SQL Island.
> 
> 2. Allez sur [ce site](https://sql-island.informatik.uni-kl.de/) et trouvez un moyen de vous échapper de l'île !
> 
>     *Remarque : si le site est en Allemand, cliquez sur le menu (barres horizontales en haut à gauche) puis sur "Sprache wechsein" et changez la langue.*

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : Éric DÉTREZ (memento SQL)

Inspirations : N. Pecheux
