# Implémentation d'une pile

def nouvelle_pile() :
    return []

def est_pile_vide(pile) :
    return pile == []

def empile(pile, elt) :
    pile.append(elt)
    
def depile(pile) :
    assert not est_pile_vide(pile)
    return pile.pop()


# Implémentation d'une file

def nouvelle_file() :
    return { "entree" : [] , "sortie" : [] }

def est_file_vide(file) :
    return file["entree"] == [] and file["sortie"] == []

def enfile(file, elt) :
    file["entree"].append(elt)
    
def defile(file) :
    assert not est_file_vide(file)
    
    if file["sortie"] == [] :
        file["sortie"] = file["entree"][::-1]
        file["entree"] = []
        
    return file["sortie"].pop()