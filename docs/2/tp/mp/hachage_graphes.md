# TP : Graphes implémentés par une table de hachage

Pour implémenter un graphe, vous connaissez deux représentations possibles :

* sa matrice d'adjacence (en Python, on utilise généralement une liste de listes)
* sa liste d'adjacence (en Python, on utilise généralement un dictionnaire)

> 1. Rappeler le fonctionnement d'une matrice d'adjacence.
> 2. Rappeler le fonctionnement d'une liste d'adjacence.

Dans ce TP, nous allons représenter des graphes par leurs listes d'adjacences, que nous stockerons dans des dictionnaires. Nous n'utiliserons pas le type `dict` cependant, nous allons implémenter nos propres dictionnaires !

## I. Création de nos propres dictionnaires

Nous allons supposer pour simplifier que les clés de nos dictionnaires seront toujours des chaînes de caractères.

Nous allons donc commencer par créer notre propre fonction de hachage pour les chaînes de caractères. On considère $`c_0c_1...c_{n-1}`$ une chaîne à $n$ caractères. On note $`u_i`$ le code Unicode du caractère $`c_i`$ de la chaîne. La fonction de hachage devra alors renvoyer $`\displaystyle \sum_{i=0}^{n-1} u_i \times(i+1)`$.

> 1. Tester dans la console Python :
>
>     ```python
>     >>> ord('a')
>     ?
>     >>> ord('b')
>     ?
>     >>> ord('.')
>     ?
>     ```
>
>     Que fait la fonction `ord` ?
>
> 2. Écrire une fonction `fonction_hachage(chaine)` qui prend en paramètre une chaîne de caractères et renvoie son hash tel que défini ci-dessus.
>
>     Voici quelques exemples pour tester ensuite votre fonction :
>
>     ```python
>     >>> fonction_hachage("Faidherbe")
>     4596
>     >>> fonction_hachage("1nf0rm@t1qu€")
>     106593
>     >>> fonction_hachage("!!")
>     99
>     ```
>
> 3. Peut-il y avoir des collisions ?
>
> 4. Proposer une fonction `hash_reduit(chaine, taille)` qui prend en paramètres une chaîne de caractères et la taille de la table de hachage, et renvoie l'indice où la chaîne devra être rangée dans la table.
>
>     ```python
>     >>> hash_reduit("Faidherbe", 100)
>     96
>     >>> hash_reduit("!!", 100)
>     99
>     ```
>
> 5. Cette fonction augmente-t-elle le risque de collisions ?

Nous allons maintenant créer des fonctions pour gérer nos tables de hachage. On utilisera une liste Python pour représenter une table de hachage.

On supposera pour simplifier qu'une table de hachage ne change plus de taille une fois créée (pas de redimensionnements quand la table est trop vide/pleine comme le fait Python). La taille sera fixée à la création.

Pour gérer les collisions, plutôt que d'utiliser l'*open addressing* comme le fait Python, on choisira simplement de stocker toutes les clés ayant le même hash réduit au même indice dans la table de hachage à l'aide d'une liste.

Lors de l'ajout d'une première association clé-valeur dans la table, on aura donc, au bon indice, une liste contenant un tuple (clé, valeur), et aux autres indices une liste vide. Si collision lors d'une autre insertion, on aura ensuite une liste de 2 tuples à cet indice, ...

> 6. Écrire une fonction `creation_dict(t)` qui renvoie une table de hachage de taille `t`.
>
>     ```python
>     >>> creation_dict(5)
>     [[], [], [], [], []]
>     ```
>
> 7. Écrire une fonction `insertion_dict(th, cle, val)` qui ajoute à la table de hachage `th` l'association `(cle, val)`.
>
>     ```python
>     >>> th5 = creation_dict(5)
>     >>> insertion_dict(th5, "Faidherbe", 3.14)
>     >>> th5
>     [[], [('Faidherbe', 3.14)], [], [], []]
>     ```
>
> 8. Voici une fonction qui indique si une clé est présente dans la table de hachage :
>
>     ```python
>     def presence_dict(th, cle):
>         hr = hash_reduit(cle, len(th))
>         for i in range(len(th[hr])): # on parcourt les associations stockées à l'indice hr
>             c, v = th[hr][i]
>             if c == cle:
>                 return True
>         return False
>     ```
>
>     Pourquoi n'a-t-on pas besoin de regarder à tous les indices de la table de hachage ? La complexité de cette fonction dépend-elle du nombre d'associations présentes dans la table de hachage ? de quoi dépend-elle ?
>
> 9. En vous inspirant de la fonction précédente, écrire une fonction `acces_dict(th, cle)` qui renvoie la valeur associée à la clé dans la table de hachage. On lèvera une exception (avec `raise`) de même nom que celle habituelle de Python si la clé n'est pas présente.
>
> 10. Écrire une fonction `modification_dict(th, cle, nouvelle_val)`, qui modifie la valeur associée à la clé donnée. On lèvera à nouveau la bonne exception si la clé n'est pas présente.
>
> 11. Écrire une fonction `suppression_dict(th, cle)` qui supprime l'association dont la clé est donnée. On lèvera à nouveau la bonne exception si la clé n'est pas présente.
>
> 12. Écrire une fonction `cles_dict(th)` qui renvoie la liste des clés présentes dans la table de hachage.
>
> 13. Tester toutes les fonctions avant de passer à la suite !

Dans la suite du TP, nous ne manipulerons les dictionnaires qu'avec ces 7 fonctions.

## II. Implémentation d'un graphe avec nos dictionnaires

Les graphes que nous allons utiliser sont stockés dans des fichiers textes.

La première ligne d'un fichier représentant un graphe contiendra son nombre de sommets. Les lignes suivantes auront le format « s1 ; s2 » et représenteront chacune un arc allant du sommet s1 au sommet s2.

> 1. * Télécharger (au même endroit que votre code Python) le fichier [gonp.txt](../code/gonp.txt).
>     * Dessiner le graphe correspondant.
>     * Est-il orienté ?
>     * Combien a-t-il de sommets ? d'arcs ?
>     * Quels sont les degrés sortants et entrants de chaque sommet ?
>     * Y a-t-il une boucle ? un cycle ?
>     * Donner l'ordre de visite des sommets pour un parcours en profondeur depuis le sommet "Helsinki".
>     * Donner l'ordre de visite des sommets pour un parcours en largeur depuis le sommet "Helsinki".
>
> 2. Tester les instructions suivantes, et rappeler ce qu'elles font :
>
>     ```python
>     >>> f = open("gonp.txt", "r", encoding="utf-8")
>     >>> l = f.readlines()
>     >>> f.close()
>     >>> print(l)
>     ?
>     ```
>
> 3. Tester l'instruction suivante :
>
>     ```python
>     >>> "Lille;Paris".split(";")
>     ```
>
>     Que fait la fonction `split` ?
>
> 4. Tester :
>
>     ```python
>     >>> s1 = "Faidherbe"
>     >>> s1[1:7:2]
>     ?
>     >>> s2 = "Lille;Paris\n"
>     >>> s2[0 : len(s2)-1 : 1]
>     ?
>     ```
>
>     Si `s` est une chaîne de caractères, et `d,f,p` trois entiers, que fait `s[d:f:p]` ? (pour rappel, on appelle cela du *slicing*, ou extraction de tranche).
>
> 5. Écrire une fonction `lecture_graphe(nom_fichier)` qui prend en paramètre le nom d'un fichier représentant un graphe et renvoie un couple :
>
>     * le premier élément du couple sera un entier correspondant au nombre de sommets du graphe (on rappelle que la fonction `int` permet de convertir une valeur en un entier)
>     * le second élément du couple sera une liste contenant des listes de 2 éléments [s1, s2] représentant un arc allant du sommet s1 au sommet s2.
>
>     ```python
>     >>> sommets, arcs = lecture_graphe("gonp.txt")
>     >>> sommets
>     4
>     >>> arcs
>     [['Lille', 'Helsinki'], ['Lille', 'Paris'], ['Helsinki', 'Amsterdam'], ['Amsterdam', 'Paris'], ['Helsinki', 'Lille'], ['Paris', 'Helsinki']]
>     ```
>
> 6. Compléter la fonction `liste_adjacence(nb_sommets, arcs)` suivante, qui prend en paramètres le nombre de sommets du graphe et la liste de ses arcs (telle que renvoyée par la fonction `lecture_graphe`), et renvoie sa liste d'adjacence (représentée avec nos tables de hachage !). Comme taille de la table, on choisira 2 fois le nombre de sommets du graphe pour réduire les risques de collisions tout en conservant une complexité spatiale raisonnable.
>
>     ```python
>     def liste_adjacence(nb_sommets, arcs):
>         # création de la table de hachage
>         res = creation_dict(.....)
>         for s1, s2 in arcs: # pour chaque arc allant du sommet s1 au sommet s2 :
>             if presence_dict(res, s1): # si s1 est déjà dans la liste d'adjacence, on ajoute s2 à la liste actuelle de ses successeurs
>                 .....
>             else: # sinon, on ajoute s1 dans la liste d'adjacence avec seulement s2 comme successeur
>                 .....
>         return res
>     ```
>
>     Pour tester :
>
>     ```python
>     >>> sommets, arcs = lecture_graphe("gonp.txt")
>     >>> liste_adjacence(sommets, arcs)
>     [[('Helsinki', ['Amsterdam', 'Lille'])], [], [('Amsterdam', ['Paris'])], [('Lille', ['Helsinki', 'Paris']), ('Paris', ['Helsinki'])], [], [], [], []]
>     ```

Pour certains algorithmes, il est plus efficace d'utiliser la représentation sous forme de matrice d'adjacence des graphes.

> 7. Écrire une fonction `matrice_carree(dim, val)` qui crée par compréhension une matrice (liste de listes) de taille `dim * dim` dont les coefficients sont tous `val`.
>
> 8. Écrire une fonction `indice_sommet` qui prend en paramètres la liste des sommets d'un graphe et le nom d'un sommet, et renvoie l'indice auquel se trouve le sommet dans la liste.
>
>     ```python
>     >>> indice_sommet(["Helsinki", "Amsterdam", "Lille", "Paris"], "Lille")
>     2
>     ```
>
> 9. Écrire une fonction `matrice_adjacence` qui prend en paramètre la liste d'adjacence d'un graphe (avec nos tables de hachage toujours) et renvoie la matrice d'adjacence du même graphe. On se basera sur la liste des sommets renvoyée par `cles_dict` pour déterminer quelle ligne de la matrice d'adjacence correspond à quel sommet.
>
>     ```python
>     >>> sommets, arcs = lecture_graphe("gonp.txt")
>     >>> matrice_adjacence(liste_adjacence(sommets, arcs))
>     [[0, 1, 1, 0], [0, 0, 0, 1], [1, 0, 0, 1], [1, 0, 0, 0]]
>     ```

On rappelle que calculer la *complexité spatiale* d'une fonction consiste à compter la place que prennent toutes les variables en mémoire. Pour une fonction ayant un graphe $G = (S, A)$, on exprime généralement les complexités (spatiale et temporelle) en fonction du nombre de sommets noté $|S|$ et/ou du nombre d'arcs (ou arêtes si non orienté) noté $|A|$.

> 10. Si une fonction manipule un graphe implémenté par sa matrice d'adjacence, quelle sera sa complexité spatiale ?
> 11. Si une fonction manipule un graphe implémenté par sa liste d'adjacence, quelle sera sa complexité spatiale ?

## III. Manipulations de graphes

Cette partie du TP vise à réviser quelques manipulations de graphes vues l'année dernière.

> **Caractéristiques des graphes**
>
> 1. Quelle particularité possède la matrice d'adjacence d'un graphe non orienté ?
>
> 2. Déterminer ce que renvoie la fonction suivante, et commenter les lignes du code pour détailler son fonctionnement :
>
>     ```python
>     def mystere(lst_adj): # lst_adj est la liste d'adjacence d'un graphe orienté (implémenté avec nos tables de hachage)
>         res = 0
>         liste_sommets = cles_dict(lst_adj)
>         for sommet in liste_sommets:
>             successeurs_sommet = acces_dict(lst_adj, sommet)
>             res = res + len(successeurs_sommet)
>         return res
>     ```
>
> 3. Écrire une fonction équivalente à la fonction `mystere` ci-dessus mais prenant en paramètre la matrice d'adjacence d'un graphe orienté.
>
> 4. Laquelle des deux fonctions précédentes est la plus efficace (meilleure complexité temporelle) ?
>
> 5. En supposant qu'il n'y ait aucune boucle, combien d'arcs un graphe orienté peut-il posséder au maximum (en fonction de son nombre de sommets) ? Combien d'arêtes un graphe non orienté peut-il posséder au maximum ?
>
> 6. Écrire une fonction `boucle_lst(lst_adj)` qui prend en paramètre la liste d'adjacence d'un graphe (implémenté avec nos tables de hachage) et renvoie `True` s'il possède une boucle, `False` sinon.
>
> 7. Même question pour une fonction `boucle_mat(mat_adj)`.
>
> 8. Laquelle des deux fonctions précédentes est la plus efficace (meilleure complexité temporelle) ?

> **Degrés**
>
> 1. Qu'obtient-on lorsqu'on additionne les degrés de tous les sommets d'un graphe non orienté ?
> 3. Écrire une fonction `degre_max_lst(lst_adj)` qui prend en paramètre la liste d'adjacence d'un graphe non orienté et renvoie le degré maximal de ses sommets.
> 4. Écrire de même une fonction `degre_max_mat(mat_adj)` qui prend en paramètre la matrice d'adjacence d'un graphe non orienté et renvoie le degré maximal de ses sommets.
> 5. Laquelle des deux fonctions précédentes est la plus efficace (meilleure complexité temporelle) ?
> 6. Si on appelait une de ces deux fonctions avec un graphe orienté, obtiendrait-on le degré entrant maximal ou le degré sortant maximal de ses sommets ?

> **Parcours**
>
> 1. Écrire une fonction `existe_chemin` qui prend en paramètre un graphe et une liste de sommets et détermine si le chemin représenté par cette liste existe. Vous choisirez, en justifiant votre choix, la représentation du graphe qui donne la meilleure complexité temporelle.
>
> 2. * Quelle structure de données permet d'implémenter un parcours en profondeur ? en largeur ?
>     * Récupérer le fichier contenant [l'implémentation de ces deux structures](../code/pile_file.py).
>     * À quoi sert la première ligne de la fonction `depile` ?
>
>     * Expliquer brièvement comment fonctionne l'implémentation de la file proposée.
>
> 3. Quelle est la complexité d'un parcours de graphe (avec sa liste d'adjacence) en profondeur ? en largeur ? Pour rappel, voici l'algorithme d'un parcours :
>
>    ```
>    PARCOURS (graphe G, sommet DEPART) :
>    	visites <- dictionnaire vide
>    	a_traiter <- pile ou file vide
>    	ajouter DEPART dans a_traiter
>    	TANT QUE a_traiter est non vide :
>    		S <- enlever un sommet de a_traiter
>    		SI S n'est pas dans visites :
>    			ajouter S à visites
>    			POUR chaque voisin V de S:
>    				ajouter V à a_traiter
>    ```
>
> 4. Écrire une fonction `parcours_profondeur(lst_adj, sommet_depart)` qui affiche les sommets du graphe dans l'ordre de leur parcours.
>
> 5. Écrire de même une fonction `parcours_largeur`.
>
> 6. Adapter le parcours de votre choix afin d'écrire une fonction `connexe(lst_adj)` déterminant si un graphe non orienté est connexe.

## Pour aller plus loin

> 1. Adapter le parcours en profondeur afin de déterminer si un graphe non orienté possède un cycle impliquant le sommet de départ du parcours.
> 2. Adapter le parcours en largeur afin de renvoyer les composantes connexes d'un graphe non orienté, sous la forme d'un dictionnaire qui à chaque sommet associe un entier symbolisant la composante à laquelle il appartient.
> 3. Reprendre les fonctions du TP et donner les variants de boucles permettant de montrer la terminaison de vos fonctions, les invariants permettant de montrer la correction de vos fonctions, et leurs complexités temporelles et spatiales.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
