# TP : Réduction d'images

L’objectif de ce TP est d’étudier diverses méthodes permettant de réduire l'espace mémoire occupé par une image.

## I. Rappels sur les manipulations d'images

Une image est une **matrice de pixels**.

Dans ce TP :

*   on utilise une liste de listes pour représenter une matrice ;
*   on utilise un tuple `(r, v, b)` pour représenter un pixel.

Un pixel est un triplet formé de 3 entiers compris **entre 0 et 255** inclus, désignant les niveaux de **rouge, vert et bleu** du pixel. Par exemple, `(0, 255, 0)` est un pixel vert, `(255, 255, 0)` est un pixel jaune, `(0, 0, 0)` est un pixel noir, …

Voici une petite image :

```python
mystere = [ [(0, 0, 255), (0, 0, 255), (255, 255, 255), (255, 255, 255), (255, 0, 0), (255, 0, 0)],
            [(0, 0, 255), (0, 0, 255), (255, 255, 255), (255, 255, 255), (255, 0, 0), (255, 0, 0)],
            [(0, 0, 255), (0, 0, 255), (255, 255, 255), (255, 255, 255), (255, 0, 0), (255, 0, 0)] ]
```

>   1.   De quelle couleur est le pixel `(0, 0, 255)` ? et `(255, 255, 255)` ? et `(255, 0, 255)` ? Que semble donc représenter l’image `mystere` ?
>
>   2.   Écrivez une fonction `dim(img)` qui renvoie le couple (hauteur, largeur) des dimensions de l’image passée en paramètre. Par exemple :
>
>        ```python
>        >>> dim(mystere)
>        (3, 6)
>        ```
>
>   3.   Si `i` et `j` sont des coordonnées valides d’un pixel d’une image `img`, que fait l’instruction suivante ?
>
>        ```python
>        r, v, b = img[i][j]
>        ```
>
>        Cette syntaxe est recommandée pour pour « dépaqueter » les pixels dans vos fonctions de la suite du TP.

Nous utiliserons la bibliothèque `matplotlib` pour la lecture des images et leur affichage.

>   4.   Ajoutez la ligne suivante en haut de votre fichier Python. À quoi sert-elle ?
>
>        ```python
>        import matplotlib.pyplot as plt
>        ```
>
>   5.   Visualisez l'image `mystere` à l'aide des instructions suivantes :
>
>        ```python
>        >>> plt.imshow(mystere)
>        >>> plt.show()
>        ```
>

Nous allons écrire plusieurs algorithmes qui effectuent un certain traitement sur une image. Pour ne pas modifier l’image d’origine, nous aurons parfois besoin d’en faire une copie.

>   6.   Écrivez une fonction `copie(img)` qui prend en paramètre une image et renvoie une copie indépendante en mémoire de celle-ci (c'est-à-dire qu'ensuite, modifier l'une ne doit pas modifier l'autre).

Pour pouvoir visualiser en parallèle l'image d'origine et l'image modifiée par nos traitements, on utilisera la fonction suivante :

```python
def affichage(image_origine, nouvelle_image):
     plt.figure()
     plt.subplot(211)
     plt.imshow(image_origine, cmap='gray')
     plt.subplot(212)
     plt.imshow(nouvelle_image, cmap='gray')
     plt.show()
```

>   7.   Copiez la fonction `affichage` dans votre fichier Python.

La fonction suivante permet de récupérer une image depuis un fichier :

```python
def lecture(nom_image):
    return [[(int(pixel[0]*255), int(pixel[1]*255), int(pixel[2]*255)) for pixel in ligne] for ligne in plt.imread(nom_image).tolist()]
```

>   8.   Copiez la fonction `lecture` dans votre fichier Python.

On travaillera principalement sur l'image suivante :

![](../img/reduction_images/montgolfieres/montgolfieres.png)

Cette image est choisie car elle donne des résultats visuellement parlant pour nos algorithmes, mais vous pourrez ensuite utiliser n'importe quelles images.

>   9.   Téléchargez l'image des [montgolfières](../img/reduction_images/montgolfieres/montgolfieres.png) dans le même répertoire que votre fichier Python, puis ouvrez là avec la fonction `lecture` dans une variable `montgolfieres`. Combien de pixels l’image possède-t-elle ?

Nous allons commencer par quelques traitements simples sur les pixels de l’image. Les fonctions ne doivent pas modifier l’image passée en paramètre.

>   10.   Écrivez une fonction `niveaux_de_gris(img)` qui renvoie une image dont les trois composantes de chaque pixel sont la moyenne des niveaux de rouge, vert et bleu du pixel correspondant de l’image passée en paramètre. Par exemple, `affichage(montgolfieres, niveaux_de_gris(montgolfieres))` donne :
>
>         ![](../img/reduction_images/montgolfieres/filtres/niveaux_de_gris.png)
>         
>   11.   Écrivez une fonction `noir_et_blanc(img, seuil)` qui renvoie une image dont les pixels sont noirs si la moyenne des niveaux de rouge, vert et bleu du pixel correspondant de l’image passée en paramètre est strictement inférieure au seuil, et blancs sinon. Par exemple, `affichage(montgolfieres, noir_et_blanc(montgolfieres, 128))` donne :
>
>         ![](../img/reduction_images/montgolfieres/filtres/noir_et_blanc.png)

On rappelle qu’en binaire, l’écriture $`\overline{b_{n-1}\dots b_1 b_0}^2`$ représente l’entier naturel $`\displaystyle\sum_{k=0}^{n-1}b_i\times 2^i`$.

Par exemples, $`4 = \overline{100}^2,\; 11 = \overline{1011}^2`$.

>   12.   Quel est l’entier naturel représenté par $`\overline{100100}^2`$ ? Quelle est l’écriture binaire de 42 ?

On appelle *filtre* un entier naturel dont l’écriture binaire comporte trois bits (quitte à compléter par des 0 à gauche s’il y en a moins). Par exemple, 3 est un filtre (écriture $`\overline{011}^2`$).

L’écriture binaire d’un filtre comporte donc 3 bits $`\overline{b_2b_1b_0}^2`$, correspondant chacun à une composante du pixel (rouge pour $`b_2`$, vert pour $`b_1`$, bleu pour $`b_0`$). Si le bit vaut 1, alors la composante doit être conservée sans modification, et si le bit vaut 0 alors la composante doit devenir 0.

Par exemple, si le pixel est (100, 36, 211) et le filtre est 3, alors le pixel devient (0, 36, 211).

>   13.   Quels sont les entiers naturels représentables sur 3 bits ?
>
>   17.   Écrivez une fonction `ecriture(n)` qui renvoie, sous forme d’un triplet de bits, l’écriture binaire de l’entier naturel `n` passé en paramètre. On lèvera une exception si l’entier n’est pas représentable sur 3 bits. Vous pouvez aussi renvoyez une liste.
>
>         ```python
>         >>> ecriture(4)
>         (1, 0, 0)
>         >>> ecriture(3)
>         (0, 1, 1)
>         >>> ecriture(0)
>         (0, 0, 0)
>         ```
>
>   18.   Écrivez une fonction `applique_filtre_pixel(pixel, filtre)` qui applique le filtre donné (un entier naturel dont l’écriture binaire contient 3 bits) au pixel donné.
>
>         ```python
>         >>> applique_filtre_pixel((100, 36, 211), 3)
>         (0, 36, 211) 
>         ```
>
>   19.   Écrivez une fonction `applique_filtre(img, filtre)` qui applique le filtre à chaque pixel de l’image. Par exemple, `affichage(montgolfieres, applique_filtre(montgolfieres, 5))` donne :
>
>         ![](../img/reduction_images/montgolfieres/filtres/filtre_5.png)
>
>   20.   Quel filtre faut-il appliquer pour que l’image ne soit pas modifiée ? Et pour qu’elle soit intégralement en niveaux de vert ?



## II. Réduction de l'espace de couleurs

Pour réduire l’espace mémoire occupé par une image, une première technique consiste à modifier le nombre de pixels différents présents dans l’image.

>   1.   Une composante (rouge, vert ou bleue) est un entier compris entre 0 et 255. Combien de bits sont donc nécessaires pour représenter une composante d’un pixel ? Combien de bits sont donc nécessaires pour représenter un pixel complet ?

Si on réduit le nombre de couleurs différentes présentes dans l'image, il sera alors possible de stocker ces couleurs sur moins de bits.

Par exemple, si on réduit l'espace de couleurs d'une image à uniquement 2 couleurs, alors on peut représenter chaque pixel par un 0 (pour la première couleur possible) ou par un 1 (deuxième couleur possible), et ainsi on a besoin que d'un seul bit par pixel.

Notre objectif est de réduire l'espace de couleurs de l'image à `k` couleurs. Les `k` couleurs devront être choisies pour conserver au maximum les nuances de l'image. Par exemple :

![](../img/reduction_images/ada.png){width=40%}

Pour réduire l’image à `k` couleurs différentes uniquement, nous allons utiliser l'algorithme des `k`-moyennes. L'objectif est de regrouper les pixels de l'image en `k` classes (les pixels d'une même classe se ressemblant). Dans l'image réduite, les pixels d'une même classe auront alors la même couleur.

>   2.   À quel type d’algorithmes d’apprentissages appartient l’algorithme des `k`-moyennes ?
>
>   3.   Pourquoi n’est-il pas nécessaire de normaliser les pixels ?
>
>   4.   Écrivez une fonction `distance_euclidienne(pixel1, pixel2)` qui renvoie la distance euclidienne entre les deux pixels `pixel1` et `pixel2`.
>
>        ```python
>        >>> distance_euclidienne((118, 126, 132), (42, 236, 102))
>        137.02554506368512
>        ```
>
>   5.   Écrivez une fonction `liste_pixels(img)` qui renvoie une liste contenant l’intégralité des pixels de l’image. Si `img` est de hauteur $h$ et de largeur $l$, `len(liste_pixels(img))` doit donc valoir $`h\times l`$.
>
>        ```python
>        >>> len(liste_pixels(montgolfieres))
>        156500
>        ```

On suppose désormais travailler avec une liste `pixels` récupérée grâce à la fonction précédente. Une boucle « `for pixel in pixels` » permet de balayer tous les pixels de l'image plus rapidement et facilement qu'avec la double boucle traditionnelle.

>   6.   Écrivez une fonction `centre(pixels)` qui prend en paramètre une liste de pixels et renvoie le centre de la classe représentée. Le “centre” est un pixel donc chaque composante est la moyenne des valeurs de cette composante pour les pixels de la liste.
>
>        ```python
>        >>> centre(liste_pixels(montgolfieres))
>        (118, 126, 132)
>        ```
>
>   7.   Écrivez une fonction `initialise(pixels, k)` qui prend en paramètres les pixels d'une image ainsi qu'un entier `k > 0`, et renvoie une liste de `k` pixels aléatoires de l'image. On pourra utiliser la fonction `sample` du module `random`.
>
>   8.   Écrivez une fonction `associe(pixel, centres)` qui renvoie l'indice du pixel de la liste `centres` le plus proche (en terme de distance euclidienne) avec le pixel `pixel`.
>
>        ```python
>        >>> associe((100, 36, 211), [(0, 136, 11), (120, 236, 111), (255, 255, 211), (99, 42, 236), (88, 197, 142)])
>        3
>        ```
>
>   9.   Écrivez une fonction `construit_classes(pixels, centres)` qui renvoie une liste de listes : à l'indice `i`, on aura la liste des pixels dont le centre le plus proche est `centres[i]`.  
>        Un exemple partiel (le résultat est trop grand pour être recopié ici) :
>
>        ```python
>        >>> c = construit_classes(liste_pixels(montgolfieres), [(0, 136, 11), (120, 236, 111), (255, 255, 211), (99, 42, 236), (88, 197, 142)])
>        >>> len(c)
>        5
>        >>> c[0][:10]
>        [(62, 75, 96), (52, 65, 94), (46, 60, 81), (43, 54, 65), (77, 69, 62), (35, 33, 33), (41, 39, 40), (55, 47, 47), (51, 43, 39), (5, 6, 9)]
>        >>> c[1][:10]
>        [(168, 172, 123), (175, 153, 36), (187, 159, 21), (165, 130, 12), (231, 214, 52), (236, 209, 69), (148, 117, 26), (219, 188, 42), (190, 154, 3), (175, 156, 57)]
>        ```
>
>   10.   Écrivez une fonction `k_moyennes(pixels, k)` qui applique l'algorithme des `k`-moyennes et renvoie les `k` centres obtenus. Pour rappel, on tire aléatoirement `k` pixels (centres provisoires). Puis, tant que les centres ne se stabilisent pas, on construit les classes et on recalcule les nouveaux centres.  
>         *Conseil : prenez une très petite valeur de `k` pour tester, l’algorithme est assez long.*
>
>   11.   Écrivez une fonction `recolorie(img, k)` qui renvoie une nouvelle image dont les pixels sont ceux de `img` mais recolorés pour n'utiliser que `k` couleurs différentes. L'algorithme des `k` moyennes nous donne les couleurs à utiliser. Ensuite, il faut parcourir l'image et recolorer chaque pixel en la couleur qui lui ressemble le plus.

Voici ce que j'ai obtenu avec `k = 8` couleurs sur les montgolfières (environ 45 secondes de calcul) :

![](../img/reduction_images/montgolfieres/k_moyennes/8_couleurs.png)

Et avec `k = 20` couleurs (environ 4 minutes de calcul) :

![](../img/reduction_images/montgolfieres/k_moyennes/20_couleurs.png)

Comme l'algorithme des `k`-moyennes *converge vers des minima locaux* (les classes dépendent du tirage aléatoire initial), il est possible que vous obteniez un résultat légèrement différent.



## III. Redimensionnement intelligent (*seam carving*)

Une autre possibilité, pour que l’image prenne moins de place en mémoire, est de supprimer certains pixels de l’image. On s'intéressera uniquement à la réduction de la largeur (et pas la hauteur) de l'image).

*Seam Carving* est un algorithme de réduction d'image « intelligent » qui tient compte du contenu, pour préserver les zones contrastées (haute énergie) au détriment des zones plus homogènes (basse énergie) pour ne pas déformer l’image. La recherche des chemins à supprimer, appelés « coutures », utilise les principes de la programmation dynamique. Une « couture » est un chemin allant du bas au haut de l’image et qui ressemble beaucoup aux chemins voisins (peu de différences de couleurs avec les pixels autour). 

De tels coutures sont illustrés en rouge ci-dessous :

![](../img/reduction_images/montgolfieres/seam_carving/200_coutures.png)

Leur suppression donne une image pour laquelle les éléments importants (les montgolfières) ne sont pas déformés :

![](../img/reduction_images/montgolfieres/seam_carving/200_coutures_supprimees.png)

On note $`\overline{\texttt{img}[i][j]}`$ la moyenne des composantes du pixel de la ligne $i$, colonne $j$ de l’image `img`.

On peut détecter les contours des objets présents dans une image en calculant la norme au carré du gradient local, appelée *énergie*, notée $E$.

Soient $i$ et $j$ les coordonnées d’un pixel de l’image `img`.

*   Pour les pixels sur les bords gauche et droit, on définit l'énergie par $`E[i][j] = +\infty`$.
*   Pour les pixels sur le bord haut (sauf les coins), on définit l'énergie par $`E[i][j] = (\overline{\texttt{img}[i+1][j]}-\overline{\texttt{img}[i][j]})^2 + \left(\frac {\overline{\texttt{img}[i][j+1]}-\overline{\texttt{img}[i][j-1]}} 2\right)^2`$
*   Pour les pixels sur le bord bas (sauf les coins), on définit l'énergie par $`E[i][j] = (\overline{\texttt{img}[i][j]}-\overline{\texttt{img}[i-1][j]})^2 + \left(\frac {\overline{\texttt{img}[i][j+1]}-\overline{\texttt{img}[i][j-1]}} 2\right)^2`$
*   Pour tous les autres pixels, on définit l’énergie par $`E[i][j] = \left(\frac {\overline{\texttt{img}[i+1][j]}-\overline{\texttt{img}[i-1][j]}} 2\right)^2 + \left(\frac {\overline{\texttt{img}[i][j+1]}-\overline{\texttt{img}[i][j-1]}} 2\right)^2`$

>   1.   Écrivez une fonction `energies(img)` qui renvoie une matrice `E` de mêmes hauteur et largeur que `img`, contenant les énergies de chaque pixel comme définies ci-dessus.
>
>        Par exemple l’affichage des énergies des montgolfières donne :
>
>        ![](../img/reduction_images/montgolfieres/seam_carving/energies.png)

Pour préserver la structure des objets de l'image, on supprime les pixels « couture par couture ». Une couture est définie comme un ensemble « continu » de pixels, joignant un pixel du haut de l'image à un pixel du bas de l'image. Pour une image de hauteur $h$, une couture sera représentée par une liste d'indices de colonnes $`C = [j_0, j_1, ..., j_{h-1}] \text{ vérifiant }  \forall i\in[\![1, h-1]\!], \;|j_i - j_{i-1}| \leqslant 1`$.

À chaque couture `C` on associe son énergie `E(C)` comme la somme des énergies des pixels traversés : $`E(C) = \displaystyle\sum_{i=0}^{h-1}E[i][C[i]]`$.

Une couture est dite optimale si elle est de plus basse énergie parmi toutes les coutures possibles. C'est la couture optimale qu'on supprimera de l'image.

Nous allons utiliser de la programmation dynamique, en conservant une matrice `ME` des meilleures (plus basses) énergies : pour tout pixel `i, j`, `ME[i][j]` est la meilleure énergie pour une couture joignant un pixel quelconque du haut de l'image au pixel `i, j` :

On note $l$ la largeur et $h$ la hauteur de l’image. On admet la relation de récurrence suivante :

*   $`ME[i][0]=ME[i][l-1]=+\infty`$
*   $ME[0][j] = E[0][j]$
*   pour $`i\in[\![1, h-1]\!], j\in[\![1, l-2]\!], ME[i][j] = E[i][j]+\min(ME[i-1][j-1],ME[i-1][j],ME[i-1][j+1])`$


> 2. Écrivez une fonction `meilleures_energies(E)` qui renvoie `ME` (calculée à partir de `E` la matrice des énergies trouvée par la détection de contours), avec l’approche bottom-up de la programmation dynamique.
>
>     Par exemple l’affichage des meilleures énergies des montgolfières donne :
>
>     ![](../img/reduction_images/montgolfieres/seam_carving/meilleures_energies.png)
>

On cherche maintenant à retrouver la solution optimale à partir de l'information calculée, c'est-à-dire à trouver la couture à supprimer depuis la matrice `ME`.

>   3.   Écrivez une fonction `couture(ME)` qui renvoie, à partir de la matrice `ME`, une couture `C` d'énergie minimale sous la forme d'une liste contenant les indices de colonnes successifs. Il faut donc trouver le pixel sur le bord bas de l’image de meilleure énergie (minimale), puis remonter à partir de ce pixel sur le voisin (parmi les 3 juste au dessus) de meilleure énergie, et ainsi de suite jusqu’en haut.
>
>   8.   La fonction suivante permet de renvoyer une copie de l'image `img` dont la couture `C` a été colorée en rouge :
>
>        ```python
>        def couture_visible(img, C):
>            res = copie(img)
>            for i in range(len(C)):
>                res[i][C[i]] = (255, 0, 0)
>            return res
>        ```
>
>        Vous pouvez vérifier que vous retrouvez bien la couture suivante :
>
>        ![](../img/reduction_images/montgolfieres/seam_carving/1_couture.png)
>
>   9.   Écrire une fonction `supprime_couture(img, C)` qui supprime la couture de l’image passée en paramètre (on modifie l’image elle-même, rien n’est renvoyé).
>
>   10.   En déduire une fonction `seam_carving(img, n)` qui retire `n` coutures de l'image `img`, en recalculant à chaque étape `E`, `ME`, et `C`.

En supprimant 200 coutures de l’image des montgolfières, j’obtiens (en environ 30 secondes) :

![](../img/reduction_images/montgolfieres/seam_carving/200_coutures_supprimees.png)

On remarque que les montgolfières se sont "rapprochées" mais sont restées intactes (pas de déformations).


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

*Source des images : Wikipedia (Ada), production personnelle (montgolfières)*
