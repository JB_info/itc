# TP : Algorithme des `k` plus proches voisins

L'objectif de ce TP est d'appliquer l'algorithme des `k`-plus proches voisins afin de simuler le fonctionnement du « Choixpeau » de la saga Harry Potter.

Pour ceux qui ne connaissent pas, Poudlard est une école de magie dans laquelle les jeunes sorciers sont répartis en quatre maisons : Gryffondor, Poufsouffle, Serdaigle, Serpentard. À leur arrivée dans l'école, c'est le Choixpeau magique qui détermine dans quelle maison placer chaque élève. Pour cela, il se base sur les vertus de l'élève, ses projets, ses centres d'intérêt, etc.

> 1. Indiquez en justifiant brièvement à quel type d'apprentissage (supervisé, non supervisé, par renforcement) l'algorithme des `k`-plus proches voisins appartient.

Pour nos algorithmes d'apprentissage :

* la *classe* sera la maison dans laquelle placer le sorcier ;
* les *caractéristiques* des sorciers seront :
    * leur nom : une chaîne de caractères ;
    * leur courage : un entier entre 0 et 10 (plus le sorcier est courageux, plus ce nombre est élevé) ;
    * leur loyauté : un entier entre 0 et 10 (plus le sorcier est loyal, plus ce nombre est élevé) ;
    * leur sagesse : un entier entre 0 et 10 (plus le sorcier est sage, plus ce nombre est élevé) ;
    * leur malice : un entier entre 0 et 10 (plus le sorcier est malin, plus ce nombre est élevé) ;
    * le nombre de générations de sorciers dans la famille : un entier positif ;
    * le résultat à un test de culture générale sur la sorcellerie : un flottant entre 0 et 100 ;
    * le nombre moyen de bonnes actions réalisées par année de leur 6 à leur 11 ans : un flottant positif ;
    * le nombre moyen de mensonges dits par année de leur 6 ans à leur 11 ans : un flottant positif ;
    * leur âge (nombre de mois) lors de leur inscription au livre d'admission de Poudlard : un entier entre 1 et 132.

Toutes ces caractéristiques sauf le nom du sorcier serviront à déterminer quelle est leur maison à Poudlard.

> 2. Téléchargez le fichier suivant : [choixpeau.csv](../code/choixpeau.csv), et placez-le dans le même répertoire que votre fichier Python.

## I. Préparation des données

Avant d'appliquer nos algorithmes, nous allons d'abord chercher à lire les données stockées dans le fichier [choixpeau.csv](../code/choixpeau.csv) et à les normaliser.

On souhaite récupérer :

* un grand dictionnaire `caracteristiques_brutes` associant, à chaque prénom de sorcier, un dictionnaire associant à chaque caractéristique sa valeur pour le sorcier en question ;
* un second dictionnaire `classes` associant, à chaque prénom de sorcier, sa maison à Poudlard.

Pour simplifier les traitements, on considérera que toutes les caractéristiques sont des flottants.

Cela donne pour les premières données :

```python
>>> caracteristiques_brutes
{'Adrian': {'Courage': 9.0, 'Loyauté': 4.0, 'Sagesse': 7.0, 'Malice': 10.0, 'Générations': 3.0, 'Test': 62.1, 'BA': 2.4, 'Mensonges': 18.3, 'Age': 87.0}, 'Andrew': ...}
>>> classes
{'Adrian': 'Serpentard', 'Andrew': ...}
```

> 1. Copiez dans votre fichier Python la fonction `lecture_caracteristiques` suivante, qui construit le dictionnaire `caracteristiques_brutes` décrit ci-dessus à partir du fichier [choixpeau.csv](../code/choixpeau.csv) :
>
>     ```python
>     def lecture_caracteristiques():
>         # lecture des données
>         f = open("choixpeau.csv", "r", encoding="utf-8") # "r" = mode lecture (read)
>         lignes = f.readlines() # liste de toutes les lignes du fichier
>         f.close()
>     
>         # noms_caracteristiques = liste des noms des caractéristiques
>         # (présents dans la première ligne du fichier)
>         noms_caracteristiques = lignes[0].split(";")
>     
>         # on ajoute un à un un les sorciers au dictionnaire caracteristiques_brutes
>         caracteristiques_brutes = {}
>         for i in range(1, len(lignes)): # on ignore la ligne 0 qui contient juste les noms des caractéristiques
>             donnees = lignes[i].split(";") # sépare la ligne selon les ";" et donne une liste
>             nom = donnees[0] # le nom est dans la première colonne
>     
>             # on ajoute dans le dictionnaire caracteristiques une a une toutes les valeurs pour le sorcier actuel
>             caracteristiques = {}
>             for i in range(1, len(noms_caracteristiques)-1): # la première et dernière colonnes sont à ignorer (il s'agit du nom et de la maison)
>                 caracteristiques[noms_caracteristiques[i]] = float(donnees[i])
>             caracteristiques_brutes[nom] = caracteristiques
>     
>         return caracteristiques_brutes
>     ```
>
> 2. Copiez et complétez la fonction suivante, qui construit le dictionnaire `classes` décrit ci-dessus à partir du fichier [choixpeau.csv](../code/choixpeau.csv) :
>
>     ```python
>     def lecture_classes():
>         # lecture des données
>         f = open(.....)
>         lignes = .....
>         f.close()
>         
>         # récupération de la maison de chaque sorcier
>         classes = {}
>         for i in range(1, len(lignes)): # on ignore la ligne 0 qui ne correspond pas à un sorcier
>             donnees = lignes[i].split(";")
>             # le nom du sorcier est la première donnée
>             nom = .....
>             # la maison du sorcier est la dernière donnée
>             maison = .....
>             # ajout dans le dictionnaire classes
>             classes[.....] = .....
>     
>         return classes
>     ```

Pour pouvoir appliquer nos algorithmes, il faudra normaliser les caractéristiques. Le but est de construire, à partir du dictionnaire `caracteristiques_brutes`, un dictionnaire `caracteristiques` au même format mais dont les valeurs ont été normalisées.

> 3. Justifiez l'intérêt ici de normaliser les caractéristiques des sorciers.
>
> 4. Copiez et complétez la fonction suivante, qui renvoie la moyenne des valeurs prises par la caractéristiques `car` pour les sorciers dans `caracteristiques_brutes` :
>
>     ```python
>     def moyenne(caracteristiques_brutes, car):
>         res = 0
>         for sorcier in caracteristiques_brutes:
>             res += ....
>         return res / .....
>     ```
>
>     Exemples :
>
>     ```python
>     >>> moyenne(lecture_caracteristiques(), 'Sagesse')
>     6.36
>     >>> moyenne(lecture_caracteristiques(), 'Age')
>     82.7
>     ```
>
> 5. Écrivez une fonction `ecart_type(caracteristiques_brutes, car)` qui renvoie l'écart-type des valeurs prises par la caractéristiques `car` pour les sorciers dans `caracteristiques_brutes`. Exemples :
>
>     ```python
>     >>> ecart_type(lecture_caracteristiques(), 'Sagesse')
>     2.6963679274164343
>     >>> ecart_type(lecture_caracteristiques(), 'Age')
>     33.221830172343
>     ```
>
> 6. Copiez et complétez la fonction suivante, qui renvoie le dictionnaire `caracteristiques` des données normalisées :
>
>     ```python
>     def normalise(caracteristiques_brutes):
>         caracteristiques = {}
>         # pour chaque sorcier
>         for sorcier in caracteristiques_brutes:
>             caracteristiques[sorcier] = {}
>             # pour chaque caractéristique du sorcier
>             for car in caracteristiques_brutes[sorcier]:
>                 # la donnée est remplacée par sa valeur centrée-réduite
>                 caracteristiques[sorcier][car] = .....
>         return caracteristiques
>     ```
>
>     Exemple :
>
>     ```python
>     >>> normalise(lecture_caracteristiques())
>     {'Adrian': {'Courage': 0.8302049262234619, 'Loyauté': -0.8926084406591749, 'Sagesse': 0.2373563316387706, 'Malice': 1.29623784782714, 'Générations': -0.23232814159820012, 'Test': 0.09407596040061762, 'BA': -0.8613076089380999, 'Mensonges': 1.393250165612711, 'Age': 0.12943296554383463}, 'Andrew': ...}
>     ```
>
> 7. Placez les lignes suivantes dans votre fichier :
>
>     ```python
>     classes = lecture_classes()
>     caracteristiques = normalise(lecture_caracteristiques())
>     drago = {'Courage': -0.9263851273798308, 'Loyauté': -0.1347945947452779, 'Sagesse': -0.8601917544331196, 'Malice': 0.527016401328286, 'Générations': 2.6566302998764924, 'Test': 0.9349317909939047, 'BA': -0.8466858589211218, 'Mensonges': 0.25303576037325276, 'Age': -0.11026185074393721}
>     ```

Le dictionnaire `drago` (déjà normalisé) correspond à l'élève dont la maison Poudlard est encore inconnue. On souhaite appliquer l'algorithme des `k` plus proches voisins afin de deviner la maison dans laquelle le Choixpeau magique va placer Drago.



## II. Algorithme des `k` plus proches voisins

### Calcul des distances

La première étape de l'algorithme est de calculer la distance euclidienne entre chaque sorcier de maison connue et celui de maison inconnue.

> 1. Écrivez une fonction `distance_euclidienne(sorcier1, sorcier2)`. Les paramètres sont deux dictionnaires contenant les caractéristiques de deux sorciers. Exemples :
>
>     ```python
>     >>> distance_euclidienne(caracteristiques['Bellatrix'], drago)
>     3.0734083620159183
>     >>> distance_euclidienne(caracteristiques['Luna'], drago)
>     5.030274358257345
>     ```
>
> 2. Écrivez une fonction `toutes_distances(caracteristiques, s)` qui renvoie un dictionnaire associant à chaque nom de sorcier présent dans `caracteristiques` sa distance euclidienne avec le sorcier `s`. Exemples :
>
>     ```python
>     >>> d = toutes_distances(caracteristiques, drago)
>     >>> d['Bellatrix']
>     3.0734083620159183
>     >>> d['Luna']
>     5.030274358257345
>     ```

### Recherche des plus proches sorciers

La seconde étape de l'algorithme est de trouver les `k` sorciers qui ressemblent le plus au sorcier de maison inconnue, à partir des distances calculées ci-dessus.

L'objectif est de récupérer une liste contenant les noms des sorciers sélectionnés. Cette liste sera donc de taille `k`.

> 3. Écrivez une fonction `minimum(dico)` qui renvoie la clé du dictionnaire dont la valeur associée est minimale. Exemple :
>
>     ```python
>     >>> minimum(toutes_distances(caracteristiques, drago))
>     'Montague'
>     ```
>
> 2. Écrivez une fonction `plus_proches_sorciers(distances, k)` qui prend en paramètre un dictionnaire associant à chaque nom de sorcier sa distance euclidienne avec l'inconnu, et renvoie la liste des `k` noms de sorciers de distance minimale. Il faut donc appeler `k` fois la fonction `minimum`, en supprimant ce minimum des distances pour qu'il ne soit plus sélectionné.
>
>     *Indication : On rappelle qu'on supprime une association (clé, valeur) d'un dictionnaire `d` avec l'instruction `d.pop(clé)`.*
>
>     Exemple :
>     
>     ```python
>     >>> plus_proches_sorciers(toutes_distances(caracteristiques, drago), 5)
>     ['Montague', 'Pansy', 'Bellatrix', 'Dean', 'Romilda']
>     ```

### Maison majoritaire

La troisième étape de l'algorithme consiste à chercher la maison de Poudlard majoritaire parmi celles des `k` plus proches sorciers.

S'il y a égalité, on pourra choisir arbitrairement la maison parmi celles majoritaires.

> 5. Écrivez une fonction `compte_maisons(sorciers, classes)` qui renvoie un dictionnaire associant à chaque maison le nombre de sorciers de la liste `sorciers` y appartenant. Le dictionnaire `classes` est celui construit dans la partie I. Exemple :
>
>     ```python
>     >>> compte_maisons(['Montague', 'Pansy', 'Bellatrix', 'Dean', 'Romilda'], classes)
>     {'Serpentard': 3, 'Gryffondor': 2, 'Poufsouffle': 0, 'Serdaigle': 0}
>     ```
>
> 6. Écrivez une fonction `maison_majoritaire(dico)` qui prend en paramètre un dictionnaire dont les clés sont les maisons et les valeurs le nombre de sorciers qui y appartiennent, et renvoie le nom de la maison majoritaire. Exemple :
>
>     ```python
>     >>> maison_majoritaire({'Serpentard': 3, 'Gryffondor': 2, 'Poufsouffle': 0, 'Serdaigle': 0})
>     'Serpentard'
>     ```

### Algorithme final

> 7. Écrivez finalement une fonction `k_plus_proches_voisins(caracteristiques, classes, k, inconnu)` qui applique successivement les étapes ci-dessus pour renvoyer la maison du sorcier `inconnu`.
> 8. Avec `k = 5`, dans quelle maison le Choixpeau envoie-t-il Drago ? Et avec `k = 9` ? Peut-on déterminer avec certitude la maison de Drago ?



## III. Phase d'apprentissage : recherche du meilleur `k`

Nous allons appliquer l'algorithme à des sorciers dont nous connaissons la maison et construire la matrice de confusion afin de trouver la valeur de `k` la plus pertinente.

Pour cela, nous choisirons 15 sorciers aléatoires qui serviront de données de tests et les 35 autres seront nos données d'apprentissage.

> 1. Copiez dans votre fichier la fonction suivante, qui sépare le dictionnaire `caracteristiques` en 2 aléatoirement :
>
>     ```python
>     import random
>     def separation(caracteristiques):
>         # tire 15 noms de sorciers aléatoirement
>         tirage = random.sample(list(caracteristiques.keys()), 15)
>         tests = {}
>         apprentissages = {}
>         # pour chaque sorcier
>         for sorcier in caracteristiques:
>             # s'il a été tiré, on le place dans les données de tests
>             if sorcier in tirage:
>                 tests[sorcier] = caracteristiques[sorcier]
>             # sinon, on le place dans les données d'apprentissage
>             else:
>                 apprentissages[sorcier] = caracteristiques[sorcier]
>         return tests, apprentissages
>     ```

La matrice de confusion sera une matrice $4 \times 4$ (car il y a 4 classes).

On pourra utiliser le dictionnaire suivant pour savoir à quelle maison correspond quelle ligne/colonne de la matrice : `{"Gryffondor": 0, "Serpentard": 1, "Serdaigle": 2, "Poufsouffle": 3}`.

Par exemple, l'entier qui sera ligne 0 et colonne 2 correspondra au nombre de fois où l'algorithme a prédit la maison "Gryffondor" alors que c'était "Serdaigle".

> 2. Écrivez une fonction `matrice_confusion(classes, tests, apprentissages, k)` qui renvoie la matrice de confusion correspondante au test de l'algorithme pour la valeur `k` donnée. 

Une fois la matrice de confusion construite, nous pouvons trouver pour chaque maison son "score", que l'on définira ainsi : $`\text{score}_\text{maison} = \frac {\text{TP}} {\text{TP} + \frac 1 2 ({\text{FN + FP}})}`$, avec 

* TP (vrai positif) : nombre de prédictions correctes d’appartenance à la maison ;
* FP (faux positif) : nombre de prédictions incorrectes d’appartenance à la maison ;
* FN (faux négatif) : nombre de prédictions incorrectes de non appartenance à la maison.

> 3. Pour la matrice de confusion suivante, et la maison `Gryffondor`, donnez les valeurs de TP, FP, FN :
>
>     ```
>     [[4, 1, 0, 0],
>      [0, 2, 0, 0],
>      [1, 0, 3, 0],
>      [1, 0, 1, 2]]
>     ```

Afin d'avoir un seul indicateur de performance pour la valeur de `k`, on calcule ensuite une moyenne pondérée des scores de chaque maison : $`\frac {\displaystyle\sum_{\text{maison}} \text{score}_\text{maison} \;\times \; |\text{maison}|} {\displaystyle\sum_{\text{maison}} |\text{maison}|}`$.

Plus l'indicateur est élevé, meilleur est le `k` testé.

> 4. Complétez la fonction `indicateur_performance(matrice_confusion)` ci-dessous qui prend en paramètre une matrice de confusion et renvoie l'indicateur de performance défini ci-dessus :
>
>     ```python
>     def indicateur_performance(matrice_confusion):
>         indicateur = 0
>         for maison in range(4):
>             TP = matrice_confusion[.....][.....]
>             FP = 0
>             FN = 0
>             for autre_maison in range(4):
>                 if autre_maison != maison:
>                     FP += matrice_confusion[.....][.....]
>                     FN += matrice_confusion[.....][.....]
>             score_maison = TP / (TP + 0.5 * (FN+FP))
>             indicateur += score_maison * (TP+FN)
>         return indicateur / 15
>     ```
>
> 4. Écrivez une fonction `meilleur_k(classes, tests, apprentissages, liste_k)` renvoie le `k` ayant le meilleur indicateur de performance parmi `liste_k`, la liste des `k` potentiels.
>
> 5. Effectuez les instructions suivantes :
>
>     ```python
>     >>> tests, apprentissages = separation(caracteristiques)
>     >>> k = meilleur_k(classes, tests, apprentissages, [3,5,7,9,11,13,15])
>     >>> k_plus_proches_voisins(apprentissages, classes, k, drago)
>     ???
>     ```
>     
>     Quelle est finalement la maison de Drago ?



## IV. Algorithmes de tris

> 1. Calculez la complexité de la fonction `plus_proches_sorciers(distances, k)` de la partie II, en fonction de `n` le nombre de sorciers (autrement dit `n = len(distances)`) et de `k`.

Il y a une autre manière de récupérer les `k` sorciers les plus proches :

* On utilise un algorithme de tri afin de ranger les distances par ordre croissant.
* On récupère les noms des sorciers correspondants aux `k` premières distances.

Nous allons implémenter cette seconde approche afin de comparer sa complexité à celle de `plus_proches_sorciers`.

Il y a plusieurs algorithmes de tris mentionnés par le programme : tri à bulles, tri par sélection tri par insertion, tri fusion, tri rapide... Nous allons comparer leurs complexités afin d'en choisir un efficace.



* Le tri à bulles consiste à comparer répétitivement deux éléments consécutifs du tableau et les échanger si nécessaire. Animation illustrant ce tri :![](../img/tris/bulles.gif)
* Le tri par sélection consiste à sélectionner le plus petit élément du tableau, l'échanger avec le premier élément, puis recommencer ces deux étapes avec le reste du tableau. Animation illustrant ce tri : ![](../img/tris/selection.gif)
* Le tri par insertion consiste à regarder les éléments un par un, et à les insérer à la bonne place parmi les éléments déjà triés avant.  Animation illustrant ce tri :![](https://upload.wikimedia.org/wikipedia/commons/0/0f/Insertion-sort-example-300px.gif?uselang=fr)
* Le tri rapide consiste à :

    * Prendre un élément de la liste (généralement le premier) comme pivot, et créer deux sous-listes : une contenant les éléments inférieurs au pivot et l'autre les éléments supérieurs.
    * Appliquer récursivement le tri rapide aux deux sous-listes.
    * Concaténer la liste triée des éléments inférieurs au pivot, le pivot, la liste triée des éléments supérieurs au pivot.
* Le tri fusion consiste à :

    * Diviser la liste en deux sous-listes de tailles égales (à un élément près si la taille est impaire).
     * Appliquer récursivement le tri fusion aux deux sous-listes.
    * Fusionner intelligemment les deux sous-listes triées. Pour cela, il suffit de comparer leurs premiers éléments et de placer le plus petit en premier de la liste triée finale, et ainsi de suite.

> 2. Donnez en justifiant très brièvement la complexité de ces 5 algorithmes de tris.
>
> 2. Implémentez un de ces 5 algorithmes de tri (au choix).
>
> 3. Écrivez une fonction `plus_proches_sorciers_v2(distances, k)`. Comme nous avons un dictionnaire et qu'il nous faut une liste pour le tri, on pourra créer et trier la liste suivante :
>
>     ```python
>     l = [(distances[sorcier], sorcier) for sorcier in distances]
>     ```
>
> 4. Calculez la complexité de votre fonction, en fonction de `n` le nombre de sorciers (autrement dit `n = len(distances)`) et de `k`. 
>
> 5. En supposant avoir utilisé l'algorithme de tri le plus efficace des 5, comparez la complexité obtenue avec celle de `plus_proches_sorciers`.



## Pour aller plus loin

> 1. Implémentez les 4 autres algorithmes de tris.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
