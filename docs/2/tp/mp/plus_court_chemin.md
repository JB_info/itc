# TP : Plus court chemin dans un graphe

Ce TP s'intéresse au problème de la recherche des plus courts chemins dans un graphe.

On travaillera avec des graphes parfois orientés, parfois non orientés. Pour ne pas alourdir le texte, on amalgamera arête et arc en employant uniquement ce dernier.

## I. Plus court chemin dans un graphe non pondéré

Dans un graphe non pondéré, on appelle *distance* entre deux sommets `x` et `y`, notée $\delta(\texttt{x,y})$, la longueur (en nombre d'arcs) du plus court chemin reliant `x` à `y`. On calcule les distances en utilisant un *parcours* de graphe.

On considère par exemple le graphe orienté non pondéré suivant, appelé $`G_O`$ :

![](../img/graphes/oriente.png){width=30%}

> 1. Donnez la liste d'adjacence de $`G_O`$, stockée dans un dictionnaire Python.
>
> 4. Donnez l'ordre de visite des sommets pour un parcours en profondeur à partir du sommet 0.
> 
> 5. Donnez l'ordre de visite des sommets pour un parcours en largeur à partir du sommet 0.

On souhaite implémenter ces parcours. On rappelle l'algorithme :

```
PARCOURS (graphe G, sommet DEPART) :
	visites <- structure de données marquant les sommets comme visités (aucun au début)
	a_traiter <- structure de données contenant uniquement le sommet DEPART
	TANT QUE il reste des sommets dans a_traiter :
		S <- enlever un sommet de a_traiter
		SI S n'a pas déjà été visité :
			marquer S comme visité
			POUR chaque voisin V de S :
				ajouter V à a_traiter
```

> 4. Quelle structure de données faut-il utiliser pour `a_traiter` pour un parcours en largeur ? en profondeur ?
> 5. En supposant que toutes les opérations sur `a_traiter` soient en $`\mathcal O(1)`$, quelle est la complexité d'un parcours de graphe ?

Nous allons implémenter une pile à l'aide du type `list` de Python.

> 6. Écrivez les fonctions suivantes :
>
>     ```python
>     def nouvelle_pile() :
>         '''
>         Renvoie une pile vide.
>         '''
>         # COMPLETER ICI
>     
>     def est_pile_vide(pile) :
>         '''
>         Renvoie True si la pile est vide, False sinon.
>         '''
>         # COMPLETER ICI
>     
>     def empiler(pile, elt) :
>         '''
>         Ajoute l'élément donné au sommet de la pile.
>         Cette fonction ne renvoie rien.
>         '''
>         # COMPLETER ICI
>     
>     def depiler(pile) :
>         '''
>         Retire l'élément au sommet de la pile et le renvoie.
>         Si la pile est vide, cette fonction lève une exception.
>         '''
>         # COMPLETER ICI
>         
>     def renverser(pile):
>         '''
>         Renvoie une pile renversée par rapport à celle passée en paramètre.
>         La pile d'origine doit être vide après appel à la fonction.
>         '''
>         # COMPLETER ICI
>     ```
>
> 9. Pour tester vos fonctions, copiez ceci dans votre fichier et exécutez :
>
>     ```python
>     def tests():
>         n = 10
>         p = nouvelle_pile()
>         for i in range(n):
>             empiler(p, i)
>         p_envers = renverser(p)
>         assert(est_pile_vide(p)) # p doit être vide
>         for i in range(n):
>             assert depiler(p_envers) == i # p_envers doit contenir les éléments de p à l'envers
>         assert (est_pile_vide(p_envers)) # et rien d'autre
>                                             
>     tests() # on lance les tests, si rien ne s'affiche tout est correct !

On propose l'implémentation suivante d'une file :

```python
def nouvelle_file() :
    '''
    Renvoie une file vide.
    '''
    return { "entree" : nouvelle_pile(),
             "sortie" : nouvelle_pile() }

def est_file_vide(file) :
    '''
    Renvoie True si la file est vide, False sinon.
    '''
    return est_pile_vide(file["entree"]) and est_pile_vide(file["sortie"])

def enfiler(file, elt) :
    '''
    Ajoute l'élément donné dans la file.
    Cette fonction ne renvoie rien.
    '''
    file["entree"].append(elt)

def defiler(file) :
    '''
    Retire un élément de la file et le renvoie.
    Si la file est vide, cette fonction lève une exception.
    '''
    assert not est_file_vide(file)

    if est_pile_vide(file["sortie"]) :
        file["sortie"] = renverser(file["entree"])

    return depiler(file["sortie"])
```

> 8. * Dessinez les évolutions des piles `entree` et `sortie` lors de la suite d'opérations suivantes :
>
>        ```python
>        f = nouvelle_file()
>        enfiler(f, 1)
>        enfiler(f, 2)
>        defiler(f)
>        enfiler(f, 3)
>        enfiler(f, 4)
>        enfiler(f, 5)
>        defiler(f)
>        defiler(f)
>        enfiler(f, 6)
>        defiler(f)
>        defiler(f)
>        defiler(f)
>        ```
>
>    * A-t-on bien le comportement attendu pour une file ?
>

On a maintenant tout ce qu'il nous faut pour implémenter les parcours de graphes.

> 9. Écrivez deux fonctions `profondeur(graphe, depart)` et `largeur(graphe, depart)`. Les graphes sont représentés par leurs listes d'adjacence.
> 10. Placez un `print(s)` après avoir marqué le sommet `s` comme visité, et appelez vos fonctions sur le graphe $`G_O`$ et le sommet 0.

On souhaite, pour le graphe $`G_O`$, calculer les distances du sommet 0 à tous les autres. Pour cela, nous allons parcourir le graphe à partir du sommet 0. Au départ, on ne connaît la distance que du sommet 0 à lui-même (qui vaut 0). Lorsqu'on visite un sommet S lors du parcours, on met à jour les distances *non encore connues* de ses voisins : la distance jusqu'au voisin est la distance jusqu'au sommet S + 1.

> 11. Donnez les distances de 0 à tous les autres sommets dans le graphe $`G_O`$.
> 12. Pour calculer les distances, il faut impérativement utiliser un parcours en largeur. Modifiez la fonction `largeur` pour renvoyer un dictionnaire qui à chaque sommet `x` du graphe associe $`\delta(\texttt{DEPART,x})`$.

## II. Plus court chemin dans un graphe pondéré : algorithme de Dijkstra

> 1. Rappelez la définition de la distance entre deux sommets dans un graphe pondéré.

Les poids dans un graphe n’ont aucune raison de représenter des distances (tout dépend de l’utilisation qu’on en fait). Malgré cela, on fera l’habituel abus de langage en employant les termes « distance » et « poids » de manière interchangeable. Pour calculer les distances d'un sommet à tous les autres dans un graphe pondéré, on ne pas utiliser un simple parcours en largeur.

> 2. Justifier que la recherche du plus court chemin dans un graphe pondéré est un problème d'optimisation. Quelles sont les deux méthodes algorithmes classiques pour résoudre des problèmes d'optimisation ?

L'algorithme de Dijkstra vu l'année dernière permet de calculer les distances avec sommet de départ fixé. À chaque étape, on "fixe" le sommet de plus petite distance parmi celles déjà calculées, et on calcule les distances du départ jusqu'aux voisins de ce sommet qui ne sont pas déjà fixés.

> 3. Expliquez en quoi l'algorithme de Dijkstra appartient aux algorithmes dits « gloutons ».
> 4. Rappelez la condition que doivent respecter les poids du graphe pour que Dijkstra puisse être utilisé.

On considère le graphe suivant :

![](../img/graphes/pondere_dijkstra.png){width=40%}

Pour dérouler à la main l'algorithme de Dijkstra, on utilise généralement un tableau. Voici celui correspondant aux calculs des distances à partir du sommet 0 dans le graphe précédent :

|                                                              |   0   |           1            |            2            |           3            |            4            |            5            |
| :----------------------------------------------------------: | :---: | :--------------------: | :---------------------: | :--------------------: | :---------------------: | :---------------------: |
| Au départ, on ne connaît la distance que de 0 à 0, les autres sommets ne sont pas accessibles. | **0** |       $`\infty`$       |       $`\infty`$        |       $`\infty`$       |       $`\infty`$        |       $`\infty`$        |
| On fixe le sommet de plus petite distance parmi ceux non encore fixés : c'est 0 (de distance 0).<br />On calcule alors les distances jusqu'aux voisins de 0 non encore fixés (1, 2, et 3).<br /><br />La distance jusque 1 était de $`\infty`$, mais on peut faire mieux en venant du sommet 0 (0 + 7). On met donc à jour la distance jusque 1 comme valant 7, avec entre parenthèses le sommet d'où on vient (0).<br />Idem pour les distances jusque 2 et 3. |   -   | 7 (depuis le sommet 0) | 10 (depuis le sommet 0) | 4 (depuis le sommet 0) |       $`\infty`$        |       $`\infty`$        |
| On fixe le sommet de plus petite distance parmi ceux non encore fixés : c'est 3 (de distance 4).<br />On calcule alors les distances jusqu'aux voisins de 3 non encore fixés (1 et 4).<br /><br />La distance jusque 1 était de 7, mais on peut faire mieux en venant du sommet 3 (4 + 2). On met donc à jour la distance jusque 1 comme valant 6, avec entre parenthèses le sommet d'où on vient (3).<br />Idem pour la distance jusque 4 (4 + 8 = 12 est mieux que $`\infty`$). |   -   | 6 (depuis le sommet 3) | 10 (depuis le sommet 0) |           -            | 12 (depuis le sommet 3) |       $`\infty`$        |
| On fixe le sommet 1 (de distance 6).<br />On calcule alors les distances jusqu'aux voisins de 1 non encore fixés (2 et 4).<br />La distance jusque 2 était de 10, ce qui est mieux que ce qu'on peut faire en venant du sommet 1 (6 + 8), donc elle n'est pas mise à jour.<br />La distance jusque 4 est mise à jour (6 + 5 = 11 est mieux que 12). |   -   |           -            | 10 (depuis le sommet 0) |           -            | 11 (depuis le sommet 1) |       $`\infty`$        |
| On fixe le sommet 2 (de distance 10).<br />On calcule les distances jusqu'aux voisins de 2 non encore fixés (4 et 5). <br />La distance jusque 4 n'est pas mise à jour (10 + 10 = 20 est pire que 11), la distance jusque 5 est mise à jour (10 + 4 = 14 est mieux que $`\infty`$). |   -   |           -            |            -            |           -            | 11 (depuis le sommet 1) | 14 (depuis le sommet 2) |
| On fixe le sommet 4 (de distance 11). La distance jusque 5 (seul voisin non fixe) n'est pas mise à jour (11 + 8 = 19 pire que 14). |   -   |           -            |            -            |           -            |            -            | 14 (depuis le sommet 2) |
| On fixe le sommet 5 (de distance 14). Aucun voisin non fixé donc aucune mise à jour. |   -   |           -            |            -            |           -            |            -            |            -            |
|         Tous les sommets sont fixés, c'est terminé.          |       |                        |                         |                        |                         |                         |

> 5. Comment retrouver le plus court chemin du départ à chaque sommet avec ce tableau ? Donnez ainsi le plus court chemin du sommet 0 au sommet 4.
>
> 4. Déroulez étape par étape (à l'aide d'un tableau) l'algorithme de Dijkstra sur le graphe suivant, à partir du sommet 0 :
>
>     ![](../img/graphes/pondere_dijkstra_2.png){width=20%}

Le code de cet algorithme se base sur le même principe qu'un parcours en largeur ou en profondeur, mais on utilise une *file de priorité* comme structure de données pour stocker les sommet qu'il reste à traiter :

```
DIJKSTRA (graphe G, sommet DEPART) :
	visites <- structure de données marquant les sommets comme visités (aucun au début)
	a_traiter <- file de priorité contenant le sommet DEPART de priorité 0	
	distances <- dictionnaire qui au sommet DEPART associe 0, et aux autres sommets associe ∞
	TANT QUE il reste des sommets dans a_traiter :
		S <- enlever le sommet de priorité minimale de a_traiter
		SI S n'a pas déjà été visité :
			marquer S comme visité
			POUR chaque voisin V de S :
				SI distances[V] > distances[S] + poids(S, V) :
					distances[V] <- distances[S] + poids(S, V)
					ajouter V à a_traiter de priorité distances[V]
	RENVOYER distances
```

Nous allons implémenter une file de priorité comme étant une liste de couples (élément, priorité) rangée dans l'ordre décroissant des priorités.

> 7. Recopiez et complétez les fonctions suivantes :
>
>     ```python
>     def nouvelle_file_prio():
>         '''
>         Renvoie une file de priorité vide.
>         '''
>         # COMPLETER ICI
>     
>     def est_file_prio_vide(fp):
>         '''
>         Renvoie True si la file de priorité est vide, False sinon.
>         '''
>         # COMPLETER ICI
>     
>     def extraire(fp):
>         '''
>         Retire de la file de priorité l'élément ayant la priorité minimale.
>         L'élément est renvoyé (mais pas sa priorité).
>         On rappelle que les éléments sont rangés par ordre décroissant des priorités.
>         '''
>         # COMPLETER ICI
>     
>     def indice_insertion(fp, prio):
>         '''
>         Renvoie l'indice où devrait être inséré un élément de priorité `prio`
>         pour que la file de priorité reste rangée dans l'ordre décroissant.
>         S'il existe déjà un élément ayant cette priorité dans la file,
>         on pourra renvoyer n'importe quel indice qui convient.
>         '''
>         # COMPLETER ICI
>     
>     def inserer(fp, elt, prio):
>         '''
>         Insère un élément au bon endroit dans la file de priorité.
>         Cette fonction ne renvoie rien.
>         '''
>         indice_ou_inserer = indice_insertion(fp, prio)
>         fp.insert(indice_ou_inserer, (elt, prio))
>     ```
>
> 9. En utilisant uniquement les 4 opérations de l'interface des files de priorité, implémentez l'algorithme de Dijkstra : `dijkstra(graphe, depart)`. On rappelle que les listes d'adjacence des graphes pondérés contiennent des couples (voisin, poids), et que `float('inf')` représente $`\infty`$ en Python.
>
> 10. Testez vos fonctions sur les deux graphes pondérés donnés plus haut, dont voici les listes d'adjacence :
>
>     ```python
>     gp1 = { 0 : [(1, 7), (2, 10), (3, 4)],
>             1 : [(0, 7), (2, 8), (3, 2), (4, 5)],
>             2 : [(0, 10), (1, 8), (4, 10), (5, 4)],
>             3 : [(0, 4), (1, 2), (4, 8)],
>             4 : [(1, 5), (2, 10), (3, 8), (5, 8)],
>             5 : [(2, 4), (4, 8)] }
>                 
>     gp2 = { 0 : [(1, 9), (4, 5)],
>             1 : [(2, 1), (4, 2)],
>             2 : [(3, 4)],
>             3 : [(0, 7), (2, 6)],
>             4 : [(1, 3), (2, 9), (3, 2)] }
>     ```
>

L'algorithme $A^*$ permet d'améliorer l'algorithme de Dijkstra.

> 10. Rappelez le principe de cet algorithme. Qu'est-ce qu'une heuristique ? Que faudrait-il changer dans le pseudo-code de l'algorithme de Dijkstra pour utiliser cette heuristique ?

## III. Plus court chemin dans un graphe pondéré : algorithme de Floyd-Warshall

L'algorithme de Floyd-Warshall a pour but de déterminer les distances entre tout couple de sommets d'un graphe pondéré avec de la programmation dynamique.

Cet algorithme utilise la représentation des graphes sous forme de matrice d'adjacence.

> 1. Dessinez le graphe pondéré $`G_P`$ dont la matrice est $\begin{pmatrix} 0 & \infty & 2 & 4 \\ 1 & 0 & 5 & \infty \\ 2 & -1 & 0 & 7 \\ \infty & \infty & -3 & 0 \end{pmatrix}$.
> 2. Soit `int` un sommet intermédiaire sur le plus court chemin allant du sommet `dep` au sommet `arr`. Justifiez que les sous-chemins de `det` à `int`, et de `int` à `arr` sont aussi des plus courts chemins.

* Pour construire un chemin optimal de `dep` à `arr`, on combine deux sous-chemins optimaux allant de `dep` à `int` et de `int` à `arr`.
* Les sous-chemins optimaux (s’il ne sont pas réduits à un arc), contiennent eux_mêmes d’autres sous-chemins optimaux. Donc les sous-problèmes ne sont pas indépendants les uns des autres.

C’est donc bien un cas où la programmation dynamique est envisageable.

On note $`\delta_\texttt{k}(\texttt{i}, \texttt{j})`$ la distance du plus court chemin allant du sommet `i` au sommet `j` en ne passant que par les `k` premiers sommets (autrement dit la distance optimale en ne s'autorisant que les sommets `{0, 1, …, k-1}` comme sommets intermédiaires).

Dans une logique bottom-up, on commence par calculer les $`\delta_0`$, puis les $`\delta_1`$, etc.

> 3. Que valent les $`\delta_0`$ ?
> 5. Les $`\delta_\texttt{k}`$ étant supposées connues pour tous les couples de sommets, précisez les deux cas possibles pour construire $`\delta_\texttt{k}(\texttt{i}, \texttt{j})`$.
> 6. Justifier alors que pour $k > 0,\; \delta_k(i, j) = \min (\delta_{k-1}(i, j) , \;\delta_{k-1}(i, k-1) + \delta_{k-1}(k-1, j))$.

Le cœur de l’algorithme de Floyd-Warshall s’appuie sur la manipulation d’une suite de *matrice des distances*. Le coefficient ligne `i`, colonne `i` de ces matrices vaut $`\delta_\texttt{k}(\texttt{i}, \texttt{j})`$.

Donnons la suite de matrices des distances obtenues pour le graphe $`G_P`$ de la question 2.

* Initialement, la matrice des distances contient les $`\delta_0`$. C'est donc la matrice d'adjacence du graphe :

    $\begin{pmatrix} 0 & \infty & 2 & 4 \\ 1 & 0 & 5 & \infty \\ 2 & -1 & 0 & 7 \\ \infty & \infty & -3 & 0 \end{pmatrix}$

* À la première itération, la matrice des distances contient les $`\delta_1`$. On met donc à jour les distances pour lesquelles passer par le sommet 0 donne un meilleur chemin :

    ![](../img/graphes/floyd_warshall_1ere_it.png)
    
    Les valeurs mises à jour sont en gras. Par exemple pour aller du sommet 1 au sommet 2, la distance auparavant était de 5 (coefficient ligne 1 et colonne 2 de la matrice précédente). Mais en passant par le sommet 0, on obtient une distance de 1 + 2 (coefficient ligne 1 et colonne 0 + coefficient colonne 0 et ligne 2), le coefficient devient donc 3. La distance du sommet 1 au sommet 3 et celle du sommet 2 au sommet 3 sont également meilleures en passant par le sommet 0, donc elles sont mises à jour.

* En continuant les itérations suivantes de l'algorithme, on obtient donc la succession de matrices de distances suivantes :

    ![](../img/graphes/floyd_warshall_autres_it.png)

> 6. Appliquez l'algorithme de Floyd-Warshall (i.e. donnez les matrices des distances successives) au graphe suivant :
>
>     ![](../img/graphes/pondere_floyd_warshall.png){width=20%}
>
> 9. Écrivez une fonction `copie_matrice(mat)` qui renvoie une copie de la matrice passée en paramètre.
>
> 10. Écrivez une fonction `floyd_warshall(graphe)` qui prend en paramètre la matrice d'adjacence d'un graphe et renvoie la matrice des distances finale obtenue avec l'algorithme de Floyd-Warshall. La matrice d'adjacence ne doit pas être modifiée.
>
> 11. Testez votre fonction sur le graphe $`G_P`$.

On souhaite pour finir pouvoir trouver les plus courts chemins en plus des distances.

On va donc conserver une suite de matrices des prédécesseurs qui va varier d'une itération à l'autre en suivant le même principe que la matrice des distances. $`P_\texttt{k}(\texttt{i}, \texttt{j})`$ désigne le prédécesseur de `j` sur le chemin optimal de `i` à `j` dont les sommets intermédiaires sont dans `{0, 1, …, k-1}`.

Initialement, les seuls sommets qui ont un prédécesseur sont ceux avec une arête qui se termine sur eux : $`P_0(i,j) = \begin{cases} i \text{ si } (i, j) \in A \\ \texttt{None} \text{ sinon }\end{cases}`$.

> 10. Écrivez une fonction `predecesseurs_initiaux(graphe)` qui prend en paramètre la matrice d'adjacence d'un graphe et renvoie $`P_0`$.
> 11. Testez votre fonction sur le graphe $`G_P`$.

On a les deux mêmes cas que pour la matrice des distances pour trouver $`P_\texttt{k} \text{ pour } \texttt{k} > 0`$ :

* Si l’ajout du sommet `k − 1` ne change pas le chemin optimal, le prédécesseur de `j` ne change pas non plus.
* Sinon, le prédécesseur de `j` dans ce chemin est le même que celui de `j` dans le chemin qui va de `k − 1` à `j`.

> 12. Modifiez votre fonction `floyd_warshall` pour qu'elle renvoie la matrice des prédécesseurs en plus de celle des distances.
> 13. Écrivez une fonction `chemin_optimal(pred, dep, arr)` prenant en paramètre la matrice des prédécesseurs (après traitement par l’algorithme) ainsi que deux entiers (sommets de départ et d’arrivée), et qui renvoie en sortie la liste des sommets parcourus en suivant le chemin optimal du départ vers l’arrivée. Si aucun chemin n’existe, elle doit renvoyer `None`.

L'algorithme de Floyd-Warshall possède tout de même une limitation : il ne fonctionne pas pour les graphes qui possèdent un cycle de poids négatif.

## Pour aller plus loin

> 1. Modifiez la fonction qui calcule les distances dans un graphe non pondéré pour renvoyer un dictionnaire qui à chaque sommet `x` associe le plus court chemin allant de `DEPART` à `x`, stocké dans une liste (de taille $`\delta(\texttt{DEPART,x}) + 1`$ donc).
> 2. * Modifiez votre fonction `dijkstra` pour renvoyer également un dictionnaire `predecesseurs` tel que  `predecesseurs[i]` indique le prédécesseur du sommet `i` dans le plus court chemin allant du sommet de départ à `i`. On pourra convenir que la valeur `None` symbolise « aucun prédécesseur » (le sommet de départ aura donc `None` comme prédécesseur puisque c'est forcément le premier sommet du chemin).
>     * Écrivez une fonction qui prend en paramètre un dictionnaire `predecesseurs` tel que décrit ci-dessus et un sommet d'arrivée, et renvoie la liste des sommets du plus court chemin allant du départ à l'arrivée.
> 5. Comparez les deux algorithmes Dijkstra et Floyd-Warshall (avantages, inconvénients, complexités, ...).
> 6. Adaptez les parcours de graphes de la première partie afin de :
> 
>     * déterminer si un graphe non orienté est connexe ;
>
>     * déterminer si un graphe non orienté possède un cycle impliquant le sommet de départ du parcours ;
>    * renvoyer les composantes connexes d'un graphe non orienté, sous la forme d'un dictionnaire qui à chaque sommet associe un entier symbolisant la composante à laquelle il appartient.
> 5. Reprenez les fonctions du TP et donnez les variants de boucles permettant de montrer la terminaison de vos fonctions, les invariants permettant de montrer la correction de vos fonctions, et leurs complexités temporelles et spatiales.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*
