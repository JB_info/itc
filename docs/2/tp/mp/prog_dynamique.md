# TP : Programmation dynamique

Ce TP s'intéresse à la résolution de divers problèmes pour lesquels la programmation dynamique est particulièrement adaptée.

## I. Un peu de construction

On cherche à calculer le nombre de façons différentes qu'il existe pour construire une clôture de longueur `n > 0` en utilisant des barrières de longueur 3 ou 4 uniquement. On notera ce nombre $`C_n`$.

Par exemple, $`C_7 = 2`$ car pour construire une clôture de longueur 7, on peut commencer par placer une barrière de taille 3 puis en placer une de taille 4, ou bien commencer par une barrière de taille 4 puis finir par une de taille 3, et il n'y a aucune autre manière de construire la clôture.

> 1. De quel type de problème s'agit-il ?
> 2. Expliquez pourquoi il faut distinguer deux cas de base : le premier pour $`C_1 \text{ et } C_2`$  et le second pour $`C_3 \text{ et }C_4`$.
> 3. À la main, déterminez $`C_{10}, C_{11}, C_{12}`$.
> 4. De quel(s) sous-problème(s) va dépendre le calcul de  $`C_n`$ pour $`n > 4`$ ? Donnez alors la relation de récurrence pour calculer $`C_n`$.
> 5. Écrivez une fonction récursive simple `construction_cloture(n)` qui renvoie le nombre de façons différentes de construire une clôture de longueur `n`.
> 6. Appelez la fonction pour vérifier vos réponses de la question 3. Appelez aussi la fonction avec `n = 100`. Que se passe-t-il ?
> 7. Dessinez l'arbre d'appels de la fonction. Y a-t-il des chevauchements ? Justifiez alors que l'usage de la programmation dynamique est pertinente.

On se propose d'utiliser une liste de taille `n+1` pour stocker, à chaque indice $`0 \leqslant i \leqslant n`$, la valeur de $`C_i`$.

> 8. Écrivez une fonction `construction_cloture_top_down(n)` qui utilise de la récursivité avec mémoïsation pour renvoyer $`C_n`$.
> 10. Écrivez une seconde fonction `construction_cloture_bottom_up(n)` qui fait les calculs « de bas en haut » de manière itérative cette fois.
> 11. Appelez vos deux fonctions avec `n = 100`, a-t-on bien amélioré l'efficacité du calcul ?
> 12. Donnez la complexité spatiale de vos fonctions.


## II. Découpe optimale

Un tailleur de tissus dispose d’une long ruban de tissu de taille `T` ainsi que d’une liste de `p` pièces qu’il peut fabriquer à l’aide de ce ruban. Chacune des `p` pièces réalisables a une longueur $`l_i`$ et un prix de vente $`v_i`$ ($`0 \leqslant i < p)`$. Toutes les dimensions et prix de vente sont des entiers strictement positifs, en centimètres et euros respectivement.

On suppose que chacune des pièces peut être réalisée un nombre arbitraire de fois (éventuellement nul). On remarquera que la découpe du tissu est commutative : découper le produit `i` puis le produit `j` donne le même profit que découper le produit `j` puis le produit `i`.

Par exemple, avec `T = 16`, `p = 2`, $`l_0 = 6, v_0 = 3, l_1 = 8, v_1 = 5`$, le tailleur peut choisir de fabriquer une fois la pièce 0 et une fois la pièce 1 pour un profit de 8€ (et il lui reste 2cm de tissu).

Le tailleur doit décider quelles pièces il va réaliser pour maximiser son profit `V`.

> 1. On considère les `p = 2` pièces décrites ci-dessus ($`l_0 = 6, v_0 = 3, l_1 = 8, v_1 = 5`$). Déterminez à la main le profit maximal `V` du tailleur, pour `T = 16`, puis pour `T = 21`.
> 2. De quel type de problème s'agit-il ?
> 4. Une résolution au problème avec une stratégie « force brute » est-elle envisageable ?

Les pièces qu'il est possible de fabriquer seront stockées dans une liste de taille `p` contenant des couples $`(l_i, v_i)`$

On se propose d'utiliser une stratégie gloutonne pour répondre au problème.

À chaque étape, on choisit une pièce parmi celles de longueur inférieure à celle du ruban de tissu restant. Il y a alors plusieurs choix gloutons possibles : sélectionner à chaque étape la pièce de prix de vente maximale, ou sélectionner celle dont le ratio prix de vente / longueur est maximal, ou ...

> 4. Écrivez une fonction de comparaison `prix_vente_max(p1, p2)` qui prend en paramètre deux pièces de tissu (couples $`(l_i, v_i)`$) et renvoie :
>
>     * 0 si le prix de vente des deux pièces est égal
>     * un entier négatif si `p2` a un prix de vente supérieur à `p1`
>     * un entier positif sinon
>
> 6. Écrivez une fonction de comparaison `ratio_max(p1, p2)` qui compare cette fois en utilisant le ratio prix de vente / longueur.
>
> 7. On suppose disposer d'une fonction de comparaison `cmp` qui prend en paramètre deux éléments `a` et `b`, et renvoie :
>
>     * 0 si `a` et `b` sont égaux ;
>    * un entier négatif quelconque si `a < b` ;
>     * un entier positif quelconque sinon.
> 
>     Écrivez une fonction `tri(L, cmp)` qui trie la liste `L` en utilisant `cmp` pour comparer les éléments de `L`.
>
>     On utilisera l'algorithme de tri de son choix, et on en précisera le nom et la complexité temporelle.
>
> 8. Écrivez une fonction `decoupe_optimale_glouton(T, pieces, cmp)` qui prend en paramètre la taille `T` du ruban de tissu, la liste des pièces de tissu qu'il est possible de construire, et la fonction de comparaison qui indique quel choix glouton effectuer. On renverra un entier correspondant au profit réalisé par le tailleur avec cette stratégie gloutonne.
>
> 9. Testez votre fonction sur l'exemple donné en introduction. L'algorithme glouton renvoie-t-il ici toujours la découpe optimale avec `cmp = prix_vente_max` ? et avec `cmp = ratio_max` ?

On va maintenant utiliser la programmation dynamique pour répondre au problème.

On note $`V_t`$ le profit maximal pour un ruban de longueur `t`.

> 9. Que vaut $`V_0`$ ?
>
> 12. Justifiez la relation de récurrence suivante, pour `t > 0` :
>
>     $`V_t = \max\limits_{0 \leqslant i < p,\; l_i \leqslant t} \{V_{t-l_i} + v_i\}`$
>
> 13. Justifiez la pertinence de l'usage de la programmation dynamique.
>
> 14. Quelle structure peut-on utiliser pour la mémoïsation ? Faîtes un schéma.
>
> 15. Écrivez une fonction `decoupe_optimale_top_down(T, pieces)` utilisant de la récursivité avec mémoïsation.
>
> 16. Écrivez aussi une fonction `decoupe_optimale_bottom_up(T, pieces)` faisant les calculs de bas en haut.
>
> 17. Que valent les complexités temporelle et spatiale ? Commentez.

On veut maintenant reconstruire la solution optimale à partir du profit calculé.

On note `memo` la structure utilisée pour la mémoïsation ci-dessus. On cherche à retrouver, à partir de `memo`, la liste `decoupes` des indices des pièces à fabriquer pour réaliser le profit maximal. Il suffit pour cela de partir du profit `memo[T]` et de déterminer l'indice `i` d'une pièce telle que $`\text{memo}[T - l_i] + v_i = \text{memo}[T]`$ . On continue ensuite avec un ruban de longueur $`T - l_i`$, et ce jusqu’à ce qu’aucune pièce ne puisse plus être fabriquée.

> 16. Écrivez une fonction `reconstruit_decoupe_optimale(T, pieces, memo)` qui renvoie la liste `decoupes` définie ci-dessus.
>
> 20. Testez votre fonction sur l'exemple donné en introduction.
>
>     *Remarque :* pour récupérer `memo`, vous pouvez simplement modifier le `return` d'une de vos fonctions des questions 14 ou 15.

## III. Rendu de monnaie

On possède un système de pièces de valeurs entières $`P = \{p_0, p_1, \cdot\cdot\cdot, p_{n-1}\}, \text{ avec } p_0 = 1 \text{ et } p_0 < p_1 < \cdot\cdot\cdot < p_{n-1}`$.

Le problème du rendu de monnaie consiste, pour $`S \in \mathbb N`$, à déterminer des entiers naturels $`a_0, a_1, \cdot\cdot\cdot a_{n-1}`$ tels que $`S = \displaystyle \sum_{i = 0}^{n-1} a_ip_i`$.

Le problème possède de nombreuses solutions, l'objectif sera ici est minimiser $`\displaystyle \sum_{i=0}^{n-1}a_i`$, c'est-à-dire de rendre le moins de pièces possibles.

> 1. De quel type de problème s'agit-il ?
> 2. Proposez une fonction `rendu_glouton(S, P)` qui renvoie la liste des $`a_0, a_1, \cdot\cdot\cdot, a_{n-1}`$ en utilisant une stratégie gloutonne.
> 3. En utilisant un exemple, montrez que cette stratégie gloutonne n'est pas optimale.
> 4. On note $`N_s`$ le nombre minimal de pièces nécessaires pour rendre la somme `s`. De quels sous-problèmes dépend le calcul de $`N_s`$ ? Donnez la relation de récurrence et les cas de base.
> 5. La programmation dynamique est-elle pertinente ? Quelle structure de données peut-on utiliser pour la mémoïsation ?
> 7. Écrivez une fonction `rendu(S, P)` qui renvoie $`N_S`$ avec la stratégie de programmation dynamique de votre choix.
> 9. On souhaite reconstruire la solution au problème depuis les valeurs calculées, c'est-à-dire déterminer la liste des $`a_0, a_1, \cdot\cdot\cdot, a_{n-1}`$ en plus de $`N_S`$. Modifiez votre code en conséquence.

## Pour aller plus loin

> 1. Pour le premier problème, on souhaite maintenant, en plus de compter le nombre de façons différentes de construire une clôture de longueur `n`, énumérer aussi quelles sont ces façons. Par exemple, pour `n = 7`, on souhaite obtenir la liste `[[3, 4], [4, 3]]`.
>
>     Il faudra donc modifier la liste utilisée pour la mémoïsation pour stocker à l'indice $`0 \leqslant i \leqslant n`$ une liste de taille $`C_i`$ contenant les différentes façons de construire la clôture de taille `i`.
>
>     Modifiez ainsi vos deux fonctions `construction_cloture_top_down(n)` et `construction_cloture_bottom_up(n)`.
>
> 1. Si vous avez terminé, reprenez les divers problèmes de combinatoire / d'optimisation vus en cours et implémentez leurs résolutions en Python.
>
> 1. Reprenez vos codes et donnez les variants de boucles permettant de montrer la terminaison de vos fonctions, et les invariants permettant de montrer la correction de vos fonctions.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

