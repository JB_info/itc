# DM 2 : introduction aux bases de données 

* À rendre le **lundi 6 janvier.**
* Durée estimée : **3h**.

Ce DM est une introduction à la manipulation des **bases de données**, notre prochain (et dernier) chapitre d'ITC. Il présente les bases du langage **SQL**, qu'il faut impérativement connaître pour pouvoir faire le prochain TP, il est donc important d'étudier attentivement l'intégralité de ce DM.

Vous aurez besoin d'un ordinateur pour faire ce DM. Il faut télécharger :

* le logiciel « DB Browser for SQLite » : [https://sqlitebrowser.org/dl/](https://sqlitebrowser.org/dl/) 
* le fichier contenant la base de données : [https://framagit.org/jb_info/itc/-/blob/main/docs/2/tp/code/pizzas.db](./code/pizzas.db) .

Une fois installé, ouvrez le logiciel DB Browser puis cliquez en haut à gauche sur l'onglet « Ouvrir une base de données » et sélectionner le fichier "pizzas.db". Une fois ouvert, cliquez sur l'onglet « Parcourir les données » pour visualiser les données de la base.

Une base de données est constituée de plusieurs **tables**, contenant chacune une partie des données. La base « pizzas.db » contient trois tables :

* une table « Pizzas » qui contient les données sur les pizzas existantes ;
* une table « Clients » qui contient les données des clients de la pizzeria ;
* une table « Commandes » qui contient les données des commandes passées à la pizzeria.

Le menu déroulant en haut à gauche de DB Browser permet de sélectionner la table dont on veut afficher les données.

![](img/bdd/db_browser_intro.png)

Pour rechercher des données dans une base, on utilise des **requêtes SQL**.

Il faut aller dans l'onglet « Exécuter le SQL » de DB Browser, entrer la requête dans la zone dédiée puis exécuter en cliquant sur le triangle au dessus. Le résultat s'affiche en dessous.

![](img/bdd/db_browser_intro2.png)

L'objectif du DM est de découvrir les mots-clefs permettant d'écrire une requête SQL.

**Vous devez reporter sur le document réponse uniquement vos réponses aux questions précédées du symbole ✍️.**

### I. Sélection simple

> 1) Une requête de sélection commence toujours pas le mot clef `SELECT` suivi des informations à récupérer puis du mot-clef `FROM` suivi de la table dans laquelle chercher ces données. Tester dans DB Browser :
>
>     ```sql
>     SELECT prenom, nom
>     FROM Clients
>     ```
>
> 2) Si on souhaite récupérer toutes les données d'une table, on utilise le symbole `*`. Tester :
>
>     ```sql
>     SELECT *
>     FROM Pizzas
>     ```
>
> 3) Si on souhaite supprimer les doublons du résultat, on utilise le mot-clef `DISTINCT`. Tester :
>
>     ```sql
>     SELECT DISTINCT nom
>     FROM Clients
>     ```
>
>     Tester aussi en enlevant le `DISTINCT`.
>
> 4) ✍️ Écrire une requête SQL permettant de récupérer, sans doublon, les villes où habitent les clients de la pizzeria.
>
> 5) Si on souhaite filtrer les résultats selon une certaine condition, on utilise le mot-clef `WHERE`. Tester :
>
>     ```sql
>     SELECT nom, prix
>     FROM Pizzas
>     WHERE prix > 16
>     ```
>
> 6) On peut relier plusieurs conditions avec les opérateurs `AND`, `OR` et `NOT`. Tester :
>
>     ```sql
>     SELECT prenom, nom
>     FROM Clients
>     WHERE (nom = 'Dubois' OR nom = 'Bach') AND prenom <> 'Paul'
>     ```
>
>     Le `=` est l'opérateur d'égalité et le `<>` l'opérateur de différence.
>
> 7) ✍️ Écrire une requête SQL permettant de récupérer toutes les informations des clients qui n'habitent pas à Dijon et dont le prénom est Paul.
>

### II. Formatage

> 8. On renomme une colonne du résultat avec le mot-clef `AS`. Tester :
>
>     ```sql
>     SELECT prenom, 2025 - annee_naissance AS 'age'
>     FROM Clients
>     ```
>
> 9. On range un résultat dans l'ordre croissant ou décroissant avec `ORDER BY`. Tester les deux requêtes suivantes :
>
>     ```sql
>     SELECT *
>     FROM Pizzas
>     ORDER BY prix ASC
>     ```
>
>     ```sql
>     SELECT *
>     FROM Pizzas
>     ORDER BY prix DESC
>     ```
>
> 10. ✍️ Écrire une requête SQL permettant de récupérer les noms, prénoms et âges des clients (en renommant la colonne), rangés dans l'ordre du plus jeune au plus vieux.
>
> 11. On peut ajouter `LIMIT n` pour ne sélectionner que les `n` premiers résultats.Tester :
>
>     ```sql
>     SELECT *
>     FROM Pizzas
>     ORDER BY prix ASC
>     LIMIT 3
>     ```
>
> 12. On peut ajouter `OFFSET m` pour ignorer les `m` premiers résultats. Tester :
>
>     ```sql
>     SELECT *
>     FROM Pizzas
>     ORDER BY prix ASC
>     LIMIT 3
>     OFFSET 2
>     ```
>
> 13. ✍️ Écrire une requête SQL permettant de récupérer le nom de la quatrième pizza la plus chère.

### III. Opérations ensemblistes

> 14. On peut faire l'union de deux résultats de requêtes avec le mot-clef `UNION`. Tester :
>
>     ```sql
>     SELECT nom AS 'tous les noms et prénoms'
>     FROM Clients
>     	UNION
>     SELECT prenom
>     FROM Clients
>     ```
>
> 15. On peut aussi faire l'intersection avec `INTERSECT`. Tester :
>
>      ```sql
>      SELECT nom AS 'tous les noms qui sont aussi des prénoms'
>      FROM Clients
>      	INTERSECT
>      SELECT prenom
>      FROM Clients
>      ```
>
> 16. Enfin, on peut récupérer les résultats de la première requête privé des résultats de la deuxième avec `EXCEPT`. Tester :
>
>     ```sql
>     SELECT nom AS 'tous les noms qui ne sont pas des prénoms'
>     FROM Clients
>     	EXCEPT
>     SELECT prenom
>     FROM Clients
>     ```
>
> 17. ✍️ Écrire une requête SQL permettant de récupérer les prénoms des clients qui s'appellent comme une pizza.

### IV. Fonctions d'agrégation et groupes

> 18. On peut appliquer une fonction au résultat d'une requête : `MIN` (minimum du résultat), `MAX` (maximum du résultat), `SUM` (somme du résultat), `AVG` (moyenne du résultat) et `COUNT` (compte le nombre de lignes du résultat). Tester les cinq requêtes suivantes :
>
>     ```sql
>     SELECT MIN(annee_naissance)
>     FROM Clients
>     ```
>
>     ```sql
>     SELECT nom, MAX(prix)
>     FROM Pizzas
>     ```
>
>     ```sql
>     SELECT AVG(annee_naissance)
>     FROM Clients
>     ```
>
>     ```sql
>     SELECT SUM(prix)
>     FROM Pizzas
>     ```
>
>     ```sql
>     SELECT COUNT(nom)
>     FROM Pizzas
>     ```
>
> 19. ✍️ Écrire une requête SQL permettant de récupérer le prix moyen d'une pizza.
>
> 20. ✍️ Écrire une requête SQL permettant de récupérer le nombre de clients nés en 2000.
>
> 22. On peut regrouper les lignes du résultat qui ont la même valeur pour une colonne avec `GROUP BY`. Les fonctions d'agrégation s'appliquent alors à chaque groupe et non plus à tout le résultat. Tester ces deux requêtes :
>
>     ```sql
>     SELECT nom, COUNT(nom) AS 'nombre de clients ayant ce nom'
>     FROM Clients
>     GROUP BY nom
>     ```
> 
>     ```sql
>     SELECT nom, AVG(annee_naissance) AS 'annee de naissance moyenne pour les clients ayant ce nom'
>     FROM Clients
>     GROUP BY nom
>     ```
> 
> 23. ✍️ Écrire une requête SQL permettant de récupérer, pour chaque ville, l'âge du plus vieux client qui y habite. 
>
> 25. Il est possible de filtrer les groupes avec le mot-clef `HAVING`. Par exemple la requête suivante ne conserve que les noms appartenant à aux moins deux personnes, tester :
>
>     ```sql
>     SELECT nom, COUNT(nom) AS 'nombre de clients ayant ce nom'
>     FROM Clients
>     GROUP BY nom
>     HAVING COUNT(nom) >= 2
>     ```
> 
> 26. ✍️ Écrire une requête SQL permettant de récupérer, pour chaque ville ayant moins de 10 habitants, le nombre de clients qui y habitent.

### V. Jointures

> 25. Si on a besoin de données réparties sur plusieurs tables, il faut réaliser une jointure avec `JOIN`. La jointure se fait sur la colonne que les tables ont en commun. Ici, les tables « Pizzas » et « Commandes » ont une colonne en commun (le numéro de la pizza), on peut donc les relier ainsi, tester :
>
>     ```sql
>     SELECT nom AS 'nom de la pizza', date AS 'date de la commande'
>     FROM Pizzas
>     JOIN Commandes ON Pizzas.num = Commandes.num_pizza
>     ```
>
> 28. On peut enchaîner les jointures pour relier plus de tables, tester :
>
>     ```sql
>     SELECT Pizzas.nom AS 'nom de la pizza', Clients.nom AS 'nom du client', date AS 'date de la commande'
>     FROM Pizzas
>     JOIN Commandes ON Pizzas.num = Commandes.num_pizza
>     JOIN Clients ON Clients.id = Commandes.id_client
>     ```
>
>     Si plusieurs tables ont des colonnes qui s'appellent pareil (comme ici `nom` pour les pizzas et les clients), il faut préciser à chaque fois la table d'origine (ici `Pizzas.nom` et `Clients.nom`).
>
> 29. ✍️ Écrire une requête SQL permettant de récupérer, pour les commandes concernant une pizza de plus de 16€, le prix et nom de cette pizza avec le nom et prénom du client l'ayant commandée.
>
> 30. ✍️ On peut combiner tous les mots-clefs découverts pour écrire des requêtes SQL plus complexes. Expliquer ce que la requête SQL suivante permet de récupérer :
>
>     ```sql
>     SELECT Clients.nom, Clients.prenom
>     FROM Clients
>     JOIN Commandes ON Clients.id = Commandes.id_client
>     JOIN Pizzas ON Commandes.num_pizza = Pizzas.num
>     WHERE ville = 'Dijon'
>     GROUP BY id_client
>     HAVING COUNT(Commandes.num) >= 3
>     ORDER BY AVG(Pizzas.prix) DESC
>     LIMIT 1
>     ```


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
