# Calendrier des PC

*Remarque : Le calendrier suivant est prévisionnel, il est susceptible d'évoluer.*

Volume horaire :

* Cours : 16h réparties sur l'année
* TP : 16h réparties sur l'année, en demi-groupe

|  Semaine   |                            Cours                             |                            TP                             |                           Devoirs                            |
| :--------: | :----------------------------------------------------------: | :-------------------------------------------------------: | :----------------------------------------------------------: |
| 36 (02/09) |        1. [Principes du hachage](../cours/hachage.md)        |                             -                             | [Réviser les fondamentaux du programme d'ITC de sup](../fondamentaux.pdf) |
| 37 (09/09) | 2. [Programmation dynamique](../cours/programmation_dynamique.md) |                             -                             |                                                              |
| 38 (16/09) | 2. [Programmation dynamique](../cours/programmation_dynamique.md) | 1. [Dictionnaires et hachage](../tp/pc/dico_hachage.pdf)  |                                                              |
| 39 (23/09) | 2. [Programmation dynamique](../cours/programmation_dynamique.md) | 1. [Dictionnaires et hachage](../tp/pc/dico_hachage.pdf)  |                                                              |
| 40 (30/09) |                              -                               |                             -                             |                                                              |
| 41 (07/10) |              3. [Graphes](../cours/graphes.md)               | 2. [Programmation dynamique](../tp/pc/prog_dynamique.pdf) |                                                              |
| 42 (14/10) |              3. [Graphes](../cours/graphes.md)               | 2. [Programmation dynamique](../tp/pc/prog_dynamique.pdf) |                                                              |
|  Vacances  |                              -                               |                             -                             |                              -                               |
| 45 (04/11) |       4. [Étude des jeux](../cours/etude_des_jeux.md)        |            3. [Graphes](../tp/pc/graphes.pdf)             |                                                              |
| 46 (11/11) |       4. [Étude des jeux](../cours/etude_des_jeux.md)        |                             -                             |      DM 1 : programmation dynamique (à rendre le 14/11)      |
| 47 (18/11) |       4. [Étude des jeux](../cours/etude_des_jeux.md)        |            3. [Graphes](../tp/pc/graphes.pdf)             |                                                              |
| 48 (25/11) | - 4. [Étude des jeux](../cours/etude_des_jeux.md)<br />- 5. [Algorithmes d'apprentissage](../cours/apprentissage.md) |     4. [Floyd-Warshall](../tp/pc/floyd_warshall.pdf)      | DS 1 (30/11 - 3h) : hachage, graphes, programmation dynamique |
| 49 (02/12) |                              -                               |     4. [Floyd-Warshall](../tp/pc/floyd_warshall.pdf)      |                                                              |
| 50 (09/12) | 5. [Algorithmes d'apprentissage](../cours/apprentissage.md)  |                          5. Jeux                          |                                                              |
| 51 (16/12) | 5. [Algorithmes d'apprentissage](../cours/apprentissage.md)  |                          5. Jeux                          |                                                              |
|  Vacances  |                              -                               |                             -                             |                                                              |
| 2 (06/01)  | 5. [Algorithmes d'apprentissage](../cours/apprentissage.md)  |   6. [Tris et k plus proches voisins](../tp/pc/knn.pdf)   |                         DM 2 : jeux                          |
| 3 (13/01)  |     6. [Bases de données](../cours/bases_de_donnees.md)      |   6. [Tris et k plus proches voisins](../tp/pc/knn.pdf)   |                                                              |
| 4 (20/01)  |     6. [Bases de données](../cours/bases_de_donnees.md)      |                             -                             |                                                              |
| 5 (27/01)  |     6. [Bases de données](../cours/bases_de_donnees.md)      |                    7. Bases de données                    |     DS 2 (31/01 - 3h) : jeux, algorithmes apprentissage      |
| 6 (03/02)  |                              -                               |                    7. Bases de données                    |                                                              |
|  Vacances  |                              -                               |                             -                             |                                                              |
| 9 (24/02)  | Révisions : [Plus court chemin dans un graphe](../cours/graphes_distances.md) |                             -                             |                   DM 3 : bases de données                    |
| 10 (03/03) |                              -                               |                  8. Images et k-moyennes                  |                                                              |
| 11 (10/03) |                    Exercices de révisions                    |                  8. Images et k-moyennes                  | DS 3 (15/03 - 2h) : bases de données, graphes, programmation dynamique |
| 12 (17/03) |                    Exercices de révisions                    |                             -                             |                                                              |
| 13 (24/03) |                              -                               |                             -                             |                                                              |

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
