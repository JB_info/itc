# Calendrier des MP*

*Remarque : Le calendrier suivant est prévisionnel, il est susceptible d'évoluer.*

|                          Semaine                           |                            Cours                             |                              TP                              |                           Devoirs                            |
| :--------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
|                         37 (09/09)                         | 1. [Principes du hachage](../cours/hachage.md)<br />2. [Programmation dynamique](../cours/programmation_dynamique.md) |                                                              | [Réviser les fondamentaux du programme d'ITC de sup](../fondamentaux.pdf) |
|                         38 (16/09)                         |                                                              | 1. [Graphes implémentés par une table de hachage](../tp/mpe/hachage_graphes.md) |                                                              |
|                         39 (23/09)                         |                                                              | 1. [Graphes implémentés par une table de hachage](../tp/mpe/hachage_graphes.md) |  DS 1 (principes du hachage, graphes, algorithmique de sup)  |
|                         40 (30/09)                         | 2. [Programmation dynamique](../cours/programmation_dynamique.md) |                                                              |                                                              |
|                         41 (07/10)                         |                                                              |  2. [Programmation dynamique](../tp/mpe/prog_dynamique.md)   |                                                              |
|                         42 (14/10)                         |                                                              |  2. [Programmation dynamique](../tp/mpe/prog_dynamique.md)   |                                                              |
|                          Vacances                          |                              -                               |                              -                               |                              -                               |
|                         45 (04/11)                         |                                                              | 3. [Plus court chemin dans un graphe](../tp/mpe/plus_court_chemin.md) |      DM 1 (programmation dynamique) : à rendre le 09/11      |
| ~~46 (11/11)~~ Le cours sera rattrapé samedi 09/11 8h-10h. |       3. [Étude des jeux](../cours/etude_des_jeux.md)        |                                                              |                                                              |
|                         47 (18/11)                         |                                                              | 3. [Plus court chemin dans un graphe](../tp/mpe/plus_court_chemin.md) | DS 2 (programmation dynamique, graphes, algorithmique de sup) |
|                         48 (25/11)                         | - 3. [Étude des jeux](../cours/etude_des_jeux.md)<br />- 4. [Algorithmes d'apprentissage](../cours/apprentissage.md) |                                                              |                                                              |
|                         49 (02/12)                         |                                                              |            4. [Étude des jeux](../tp/mpe/jeux.md)            |                                                              |
|                         50 (09/12)                         |                                                              |            4. [Étude des jeux](../tp/mpe/jeux.md)            |                                                              |
|                         51 (16/12)                         | 4. [Algorithmes d'apprentissage](../cours/apprentissage.md)  |                                                              |                                                              |
|                          Vacances                          |                              -                               |                              -                               |                              -                               |
|                         2 (06/01)                          |                                                              |           5. [Bases de données](../tp/mpe/bdd.md)            | [DM 2 (bases de données) : à rendre le 06/01](../tp/intro_bdd.md) |
|                         3 (13/01)                          |                                                              |           5. [Bases de données](../tp/mpe/bdd.md)            |                                                              |
|                         4 (20/01)                          |     5. [Bases de données](../cours/bases_de_donnees.md)      |                                                              |                                                              |
|                         5 (27/01)                          |                                                              | 6. [Algorithme des `k` plus proches voisins](../tp/mpe/knn.md) |           DS 3 (bases de données, étude des jeux)            |
|                         6 (03/02)                          |                                                              | 6. [Algorithme des `k` plus proches voisins](../tp/mpe/knn.md) |                                                              |
|                          Vacances                          |                              -                               |                              -                               |                              -                               |
|                         9 (24/02)                          |                                                              |                                                              |                        Concours blanc                        |
|                         10 (03/03)                         |                                                              |    7. [Réduction d'images](../tp/mpe/reduction_images.md)    |                                                              |
|                         11 (10/03)                         |                                                              |    7. [Réduction d'images](../tp/mpe/reduction_images.md)    |                                                              |
|                         12 (17/03)                         |                          Révisions                           |                                                              |                                                              |
|                         13 (24/03)                         |                          Révisions                           |                                                              |                                                              |

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
