# Calendrier des MP*

*Remarque : Le calendrier suivant est prévisionnel, il est susceptible d'évoluer.*

Volume horaire :

* Cours : 2h de cours toutes les 3 semaines
* TP : 2h de TP toutes les 3 semaines, en demi-groupe



|    Semaine    |                            Cours                             |                              TP                              |                           Devoirs                            |
| :-----------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| 04/09 - 09/09 | 1. [Révisions générales sur les graphes](../cours/graphes_generalites.md) |                              -                               |                              -                               |
| 11/09 - 16/09 |                              -                               |              1. [Graphes](../tp/mpe/graphes.md)              |                              -                               |
| 18/09 - 23/09 |                              -                               |              1. [Graphes](../tp/mpe/graphes.md)              |                              -                               |
| 25/09 - 30/09 |                              -                               |                              -                               |           1. DS (graphes et algorithmique de sup)            |
| 02/10 - 07/10 |     2. [Bases de données](../cours/bases_de_donnees.md)      |           2. [Bases de données](../tp/mpe/bdd.md)            |                              -                               |
| 09/10 - 14/10 |                              -                               |           2. [Bases de données](../tp/mpe/bdd.md)            |                              -                               |
| 16/10 - 21/10 | - 2. [Bases de données](../cours/bases_de_donnees.md)<br />- 3. [Dictionnaires et hachage](../cours/dictionnaires_hachage.md) |                              -                               |                              -                               |
|   Vacances    |                              -                               |                              -                               |  1. DM (bases de données) : à rendre pour le mercredi 08/11  |
| 06/11 - 10/11 |                              -                               | 3. [Plus court chemin dans un graphe pondéré](../tp/mpe/plus_court_chemin.md) |                              -                               |
| 13/11 - 18/11 |                              -                               | 3. [Plus court chemin dans un graphe pondéré](../tp/mpe/plus_court_chemin.md) |                              -                               |
| 20/11 - 25/11 | 4. [Programmation dynamique](../cours/programmation_dynamique.md) |                              -                               | 2. DS (bases de données, dictionnaires et hachage, programmation dynamique, algorithmique de sup) |
| 27/11 - 02/12 |                              -                               |  4. [Programmation dynamique](../tp/mpe/prog_dynamique.md)   |                              -                               |
| 04/12 - 09/12 |                              -                               |  4. [Programmation dynamique](../tp/mpe/prog_dynamique.md)   |                              -                               |
| 11/12 - 16/12 | 5. [Algorithmes d’apprentissage](../cours/algorithmes_apprentissage.md) |                              -                               |                              -                               |
| 18/12 - 23/12 |                              -                               |    5. [Réduction d'images](../tp/mpe/reduction_images.md)    |                              -                               |
|   Vacances    |                              -                               |                              -                               | 2. DM (programmation dynamique) : à rendre pour le mercredi 17/01 |
| 08/01 - 13/01 |                              -                               |    5. [Réduction d'images](../tp/mpe/reduction_images.md)    |                              -                               |
| 15/01 - 20/01 |       6. [Étude des jeux](../cours/etude_des_jeux.md)        |                              -                               |                              -                               |
| 22/01 - 27/01 |                              -                               | 6. [Algorithmes d’apprentissage](../tp/mpe/algo_apprentissage.md) |                              -                               |
| 29/01 - 03/02 |                              -                               | 6. [Algorithmes d’apprentissage](../tp/mpe/algo_apprentissage.md) | 3. DS (dictionnaires et hachage, programmation dynamique, algorithmes d'apprentissage) |
| 05/02 - 10/02 |       6. [Étude des jeux](../cours/etude_des_jeux.md)        |                                                              |                              -                               |
| 12/02 - 17/02 |                              -                               |            7. [Étude des jeux](../tp/mpe/jeux.md)            |                              -                               |
| 19/02 - 24/02 |                              -                               |            7. [Étude des jeux](../tp/mpe/jeux.md)            |                              -                               |
|   Vacances    |                              -                               |                              -                               |                              -                               |
| 11/03 - 16/03 |                              -                               |                              -                               |                    4. DS (concours blanc)                    |
| 18/03 - 23/03 |                              -                               |          8. Exercices de révision pour les concours          |                                                              |
| 25/03 - 30/03 |                              -                               |          8. Exercices de révision pour les concours          |                                                              |

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
