# Calendrier des PC

*Remarque : Le calendrier suivant est prévisionnel, il est susceptible d'évoluer.*

Volume horaire :

* Cours : 16h réparties sur l'année
* TP : 16h réparties sur l'année, en demi-groupe



|    Semaine    |                            Cours                             |                TP                 |                           Devoirs                            |
| :-----------: | :----------------------------------------------------------: | :-------------------------------: | :----------------------------------------------------------: |
| 04/09 - 09/09 | 1. [Dictionnaires et hachage](../cours/dictionnaires_hachage.md) |                 -                 |                                                              |
| 11/09 - 16/09 |     2. [Bases de données](../cours/bases_de_donnees.md)      |    1. Dictionnaires et hachage    |                                                              |
| 18/09 - 23/09 |                              -                               |    1. Dictionnaires et hachage    |                                                              |
| 25/09 - 30/09 |                              -                               |                 -                 |                                                              |
| 02/10 - 07/10 |     2. [Bases de données](../cours/bases_de_donnees.md)      |        2. Bases de données        |                                                              |
| 09/10 - 14/10 | 3. [Révisions générales sur les graphes](../cours/graphes_generalites.md) |        2. Bases de données        |                                                              |
| 16/10 - 21/10 | 3. [Révisions générales sur les graphes](../cours/graphes_generalites.md) |                 -                 |                                                              |
|   Vacances    |                              -                               |                 -                 |                                                              |
| 06/11 - 10/11 | 4. [Programmation dynamique](../cours/programmation_dynamique.md) |            3. Graphes             |                                                              |
| 13/11 - 18/11 | 4. [Programmation dynamique](../cours/programmation_dynamique.md) |            3. Graphes             |                                                              |
| 20/11 - 25/11 | 4. [Programmation dynamique](../cours/programmation_dynamique.md) |    4. Programmation dynamique     |                                                              |
| 27/11 - 02/12 |                              -                               |    4. Programmation dynamique     |                                                              |
| 04/12 - 09/12 | 5. [Algorithmes d’apprentissage](../cours/algorithmes_apprentissage.md) |                 -                 |                                                              |
| 11/12 - 16/12 |                     [HEURE REMÉDIATION]                      |    5. Programmation dynamique     | DS 1 (algorithmique de sup, graphes, bases de données, programmation dynamique) |
| 18/12 - 23/12 | 5. [Algorithmes d’apprentissage](../cours/algorithmes_apprentissage.md) |    5. Programmation dynamique     |                                                              |
|   Vacances    |                              -                               |                 -                 |                                                              |
| 08/01 - 13/01 | - 5. [Algorithmes d’apprentissage](../cours/algorithmes_apprentissage.md) <br>- 6. [Étude des jeux](../cours/etude_des_jeux.md) |                 -                 |                                                              |
| 15/01 - 20/01 |                     [HEURE REMÉDIATION]                      |      6. Images et k-moyennes      |                                                              |
| 22/01 - 27/01 |       6. [Étude des jeux](../cours/etude_des_jeux.md)        |      6. Images et k-moyennes      |                                                              |
| 29/01 - 03/02 |       6. [Étude des jeux](../cours/etude_des_jeux.md)        |                 -                 |                                                              |
| 05/02 - 10/02 |       6. [Étude des jeux](../cours/etude_des_jeux.md)        |                 -                 |                                                              |
| 12/02 - 17/02 |                     [HEURE REMÉDIATION]                      | 7. Tris et k plus proches voisins | DS 2 (algorithmique de sup, bases de données, algorithmes d'apprentissage, programmation dynamique) |
| 19/02 - 24/02 | [Révisions d'algorithmique des graphes](../cours/graphes_algorithmiques.md) | 7. Tris et k plus proches voisins |                                                              |
|   Vacances    |                              -                               |                 -                 |                                                              |
| 11/03 - 16/03 |                    Exercices de révisions                    |              8. Jeux              |                                                              |
| 18/03 - 23/03 |                    Exercices de révisions                    |              8. Jeux              |          DS 3 (algorithmique de sup, graphes, jeux)          |
| 25/03 - 30/03 |                    Exercices de révisions                    |                 -                 |                                                              |
| 02/04 - 06/04 |                              -                               |                 -                 |                                                              |

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
